package cloud.eventpump.examples.CQRS.cqrsGenerated;

import cloud.eventpump.common.EventsApplier;
import cloud.eventpump.examples.CQRS.businessLogic.Table1;
import cloud.eventpump.examples.CQRS.businessLogic.Table1Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CQRSEventsApplier implements EventsApplier<Table1Event> {

    @Autowired
    private CQRS1Repository cqrs1Repository;

    @Autowired
    CQRSSingleEventApplier cqrsSingleEventApplier;

    @Override
    public long findLastAppliedDBId() {
        Long maxChangeId = cqrs1Repository.maxChangeId();
        return maxChangeId != null ? maxChangeId : 0L;
    }

    @Override
    public long applyEvents(List<Table1Event> list) {
        list.forEach(cqrsSingleEventApplier::applyEvent);
        return list.get(list.size() - 1).getId();
    }

}
