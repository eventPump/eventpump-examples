package cloud.eventpump.examples.CQRS.cqrsGenerated;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@ToString
class CQRS1 {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // fields required for EventPump operations
    private long table1Id;
    private long changeId;

    // payload
    private String value1;
    private String value2;
    private String value3;

    public CQRS1(long table1Id, long changeId, String value1, String value2, String value3) {
        this.table1Id = table1Id;
        this.changeId = changeId;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
    }
}
