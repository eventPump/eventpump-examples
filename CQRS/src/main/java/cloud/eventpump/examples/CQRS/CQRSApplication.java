package cloud.eventpump.examples.CQRS;

import cloud.eventpump.examples.CQRS.businessLogic.Service;
import cloud.eventpump.examples.CQRS.cqrsGenerated.CQRSService;
import cloud.eventpump.examples.CQRS.eventPump.EventPump;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


// TODO
// 1. Timestamp
// 2. Unmamage Table1

@SpringBootApplication
public class CQRSApplication implements CommandLineRunner {

    private static final long WAIT_TIME = 100;

    @Autowired
    private Service service;

    @Autowired
    private CQRSService cqrsService;

    @Autowired
    private EventPump eventPump;

    public static void main(String[] args) {
        SpringApplication.run(CQRSApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        eventPump.startServer();
        eventPump.startLocalClient();

        long aId = service.create("a");
        long bId = service.create("b");
        long cId = service.create("c");
        long dId = service.create("d");
        System.out.println("============================= After creation");
        service.showAll();
        Thread.sleep(WAIT_TIME);
        cqrsService.showAll();

        service.delete(dId);
        service.update(cId, "C-changedPayload-operation.1");
        service.update(aId, "A-changedPayload-operation.2");
        long eId = service.create("e");
        System.out.println("============================= Changes 1");
        service.showAll();
        Thread.sleep(WAIT_TIME);
        cqrsService.showAll();

        eventPump.stopLocalClient();
        System.out.println("============================= Local Client stopped");

        // operations not applied
        service.update(aId, "X");
        service.update(eId, "Y");
        System.out.println("============================= Changes 2");
        service.showAll();
        Thread.sleep(WAIT_TIME);
        cqrsService.showAll();

        Thread.sleep(WAIT_TIME);
        Thread.sleep(100);

        // client starts again
        eventPump.startLocalClient();
        System.out.println("============================= Local client restarted");
        // CQRS synchronized
        Thread.sleep(100);
        cqrsService.showAll();

        eventPump.stopLocalClient();
    }

}
