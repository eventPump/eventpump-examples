package cloud.eventpump.examples.CQRS.eventPump;

import cloud.eventpump.examples.CQRS.businessLogic.Table1Event;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Transaction;
import org.hibernate.type.Type;

import java.io.Serializable;

public class CommitInterceptor extends EmptyInterceptor {

    private static EventPump eventPump;
    private static ThreadLocal<Boolean> genericServerEventInTransaction = new ThreadLocal<>();

    @Override
    public synchronized boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        if (entity instanceof Table1Event) {
            genericServerEventInTransaction.set(true);
        }
        return false;
    }

    @Override
    public synchronized void afterTransactionCompletion(Transaction tx) {
        if (genericServerEventInTransaction.get() != null) {
            genericServerEventInTransaction.set(null);
            eventPump.commit();
        }
    }

    public static void setCommitHandler(EventPump eventPump) {
        CommitInterceptor.eventPump = eventPump;
    }


}
