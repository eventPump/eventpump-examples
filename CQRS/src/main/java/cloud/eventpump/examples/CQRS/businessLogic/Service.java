package cloud.eventpump.examples.CQRS.businessLogic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Component
public class Service {

    @Autowired
    private Table1Repository table1Repository;

    @Autowired
    private Table1EventRepository table1EventRepository;

    @Autowired
    private EntityManager entityManager;


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public long create(String payload) {
        Table1 table1 = table1Repository.save(new Table1(payload));
        saveTable1Event(table1, Operation.CREATE);
        return table1.getId();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void update(long id, String newPayload) {
        Table1 table1 = getForId(id);
        table1.setPayload(newPayload);
        saveTable1Event(table1, Operation.UPDATE);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void delete(long id) {
        Table1 table1 = getForId(id);
        saveTable1Event(table1, Operation.DELETE);
        table1Repository.delete(table1);
    }

    private void saveTable1Event(Table1 table1, Operation operation) {
        table1EventRepository.save(new Table1Event(table1.getId(), operation, table1.getPayload()));
    }

    public void showAll() {
        table1Repository.findAll().forEach(System.out::println);
    }

    private Table1 getForId(long id) {
        return table1Repository.findById(id).orElseThrow(() -> new RuntimeException("Table1 not found id:" + id));
    }

}
