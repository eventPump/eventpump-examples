package cloud.eventpump.examples.CQRS.eventPump;

import cloud.eventpump.examples.CQRS.businessLogic.Table1;
import cloud.eventpump.examples.CQRS.businessLogic.Table1Event;
import cloud.eventpump.examples.CQRS.businessLogic.Table1EventRepository;
import cloud.eventpump.examples.CQRS.businessLogic.Table1Repository;
import cloud.eventpump.examples.CQRS.cqrsGenerated.CQRSEventsApplier;
import cloud.eventpump.localClient.LocalClientConfig;
import cloud.eventpump.localClient.LocalEventsPumpClient;
import cloud.eventpump.server.EventAdapter;
import cloud.eventpump.server.EventsProvider;
import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.ServerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EventPump {

    @Autowired
    private Table1EventRepository table1EventRepository ;

    @Autowired
    private CQRSEventsApplier cqrsEventsApplier;

    private EventsPumpServer <Table1Event>eventsPumpServer;
    private LocalEventsPumpClient<Table1Event> localClient ;

    public synchronized void startServer() {
        eventsPumpServer = new EventsPumpServer<>(
                new EventsProvider<Table1Event>() {
                    @Override
                    public List<Table1Event> provideEvents(long dbId, int maxCount) {
                        return table1EventRepository.findEvents(dbId, maxCount);
                    }
                },
                new EventAdapter<Table1Event>() {
                    @Override
                    public long getEventId(Table1Event table1) {
                        return table1.getId();
                    }
                },
                new ServerConfig()
        );
        CommitInterceptor.setCommitHandler(this);
    }

    public synchronized void startLocalClient() {
        localClient = new LocalEventsPumpClient<>(cqrsEventsApplier, eventsPumpServer, new LocalClientConfig());
        localClient.start();
    }

    public synchronized void stopLocalClient() {
        localClient.close();
    }

    public synchronized void commit() {
        eventsPumpServer.commit();
    }
}
