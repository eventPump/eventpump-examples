package cloud.eventpump.examples.CQRS.businessLogic;

public enum Operation {
    CREATE, UPDATE, DELETE
}
