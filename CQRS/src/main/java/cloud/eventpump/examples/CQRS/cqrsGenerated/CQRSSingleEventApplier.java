package cloud.eventpump.examples.CQRS.cqrsGenerated;

import cloud.eventpump.examples.CQRS.businessLogic.Table1;
import cloud.eventpump.examples.CQRS.businessLogic.Table1Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class CQRSSingleEventApplier {

    @Autowired
    private CQRS1Repository cqrs1Repository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    void applyEvent(Table1Event table1Event) {
        switch (table1Event.getOperation()) {
            case CREATE:
                create(table1Event);
                break;
            case UPDATE:
                update(table1Event);
                break;
            case DELETE:
                delete(table1Event);
                break;
        }
    }

    private void create(Table1Event table1Event) {
        for (int i = 0; i < 3; i++) {
            cqrs1Repository.save(new CQRS1(
                    table1Event.getTable1Id(),
                    table1Event.getId(),
                    table1Event.getPayload(),
                    transformValue2(table1Event.getPayload()),
                    transformValue3(table1Event.getPayload())));
        }
    }

    private void update(Table1Event table1Event) {
        List<CQRS1> cqrs1s = getForOriginalId(table1Event.getTable1Id());
        cqrs1s.forEach(cqrs1 -> {
            cqrs1.setChangeId(table1Event.getId());
            cqrs1.setValue1(table1Event.getPayload());
            cqrs1.setValue2(transformValue2(table1Event.getPayload()));
            cqrs1.setValue3(transformValue3(table1Event.getPayload()));
            cqrs1Repository.save(cqrs1);
        });
    }

    private void delete(Table1Event table1Event) {
        List<CQRS1> cqrs1s = getForOriginalId(table1Event.getTable1Id());
        cqrs1s.forEach(cqrs1Repository::delete);
    }

    private List<CQRS1> getForOriginalId(long table1Id) {
        List<CQRS1> cqrs1s = cqrs1Repository.findByTable1Id(table1Id);
        if (cqrs1s.isEmpty()) {
            throw new RuntimeException("Table1 Id not found id: " + table1Id);
        }
        return cqrs1s;
    }

    private String transformValue2(String payload) {
        return "***" + payload + "***";
    }

    private String transformValue3(String payload) {
        String result = "";
        if (!payload.isEmpty()) {
            String s = payload.substring(0, 1);
            for (int i = 0; i < 5; i++) {
                result += s;
            }
        }
        return result;
    }

}
