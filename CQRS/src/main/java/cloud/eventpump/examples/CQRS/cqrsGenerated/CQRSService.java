package cloud.eventpump.examples.CQRS.cqrsGenerated;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CQRSService {

    @Autowired
    private CQRS1Repository cqrs1Repository;

    public void showAll() {
        cqrs1Repository.findAll().forEach(System.out::println);
    }

}
