package cloud.eventpump.examples.CQRS.cqrsGenerated;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CQRS1Repository extends CrudRepository<CQRS1,Long> {

    @Query(value = "SELECT max(change_Id) FROM CQRS1", nativeQuery = true)
    Long maxChangeId();

    List<CQRS1> findByTable1Id(long originalId);
}
