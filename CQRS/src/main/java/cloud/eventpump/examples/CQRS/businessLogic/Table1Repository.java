package cloud.eventpump.examples.CQRS.businessLogic;

import org.springframework.data.repository.CrudRepository;

public interface Table1Repository extends CrudRepository<Table1, Long> {

}
