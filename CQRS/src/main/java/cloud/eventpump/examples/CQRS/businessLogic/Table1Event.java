package cloud.eventpump.examples.CQRS.businessLogic;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity( name = "Table1Event")
@Data
@NoArgsConstructor
@ToString
public class Table1Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private long table1Id;
    private Operation operation;

    private String payload;

    public Table1Event(long table1Id, Operation operation, String payload) {
        this.table1Id = table1Id;
        this.operation = operation;
        this.payload = payload;
    }
}
