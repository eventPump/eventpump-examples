package cloud.eventpump.examples.CQRS.businessLogic;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface Table1EventRepository extends CrudRepository<Table1Event, Long> {

    @Query(value = "SELECT * FROM Table1Event t WHERE t.Id > :dbId ORDER BY t.Id LIMIT :maxCount", nativeQuery = true)
    List<Table1Event> findEvents(@Param("dbId") long dbId, @Param("maxCount") long maxCount);
}
