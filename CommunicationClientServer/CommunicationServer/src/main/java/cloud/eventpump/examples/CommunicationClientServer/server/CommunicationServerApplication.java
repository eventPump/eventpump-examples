package cloud.eventpump.examples.CommunicationClientServer.server;

import cloud.eventpump.server.EventAdapter;
import cloud.eventpump.server.EventsProvider;
import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.ServerConfig;
import cloud.eventpump.statisticsServer.StatisticsEventPumpServer;
import cloud.eventpump.statisticsServer.StatisticsWebConfig;
import cloud.eventpump.statisticsServer.StatisticsWebEventPumpServer;
import cloud.eventpump.tcpServer.TcpEventsPumpServer;
import cloud.eventpump.tcpServer.TcpServerConfig;
import cloud.eventpump.utils.marshaller.JacksonMarshaller;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SpringBootApplication
public class CommunicationServerApplication implements ApplicationRunner {

    private final Random random = new Random();


    public static void main(String[] args) {
        SpringApplication.run(CommunicationServerApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        EventsPumpServer<ServerEvent> eventEventsPumpServer = new EventsPumpServer<>(
                new EventsProvider<ServerEvent>() {
                    @Override
                    public List<ServerEvent> provideEvents(long dbId, int maxEventCount) {
                        return generateRandomEvents(dbId, maxEventCount);
                    }
                },
                new EventAdapter<ServerEvent>() {
                    @Override
                    public long getEventId(ServerEvent serverEvent) {
                        return serverEvent.getId();
                    }

                    @Override
                    public long getTimestampUTC(ServerEvent serverEvent) {
                        return serverEvent.getTimestampUtc();
                    }
                },
                new ServerConfig()
        );

        TcpServerConfig<ServerEvent> tcpServerConfig = new TcpServerConfig<>( new JacksonMarshaller<>());
        TcpEventsPumpServer<ServerEvent> tcpEventsPumpServer = new TcpEventsPumpServer<>(eventEventsPumpServer, tcpServerConfig);
        tcpEventsPumpServer.start();

        StatisticsEventPumpServer<ServerEvent> statisticsEventPumpServer = new StatisticsEventPumpServer<>(eventEventsPumpServer);

        StatisticsWebEventPumpServer<ServerEvent> serverEventStatisticsWebEventPumpServer = new StatisticsWebEventPumpServer<>(statisticsEventPumpServer, new StatisticsWebConfig());
        serverEventStatisticsWebEventPumpServer.start();

        for (; ; ) {
            randomSleep();
            eventEventsPumpServer.commit();
        }
    }

    private List<ServerEvent> generateRandomEvents(long dbId, int maxEventCount) {
        System.out.println("generateRandomEvents dbId=" + dbId + " maxEventCount=" + maxEventCount);
        int eventCount = 1 + random.nextInt(maxEventCount / 2);
        List<ServerEvent> resultList = new ArrayList<>();
        for (int i = 0; i < eventCount; i++) {
            long eventId = dbId + 1 + i;
            resultList.add(new ServerEvent(eventId, System.currentTimeMillis(), "Event:" + eventId));
        }
        return resultList;
    }

    private void randomSleep() {
        try {
            Thread.sleep(random.nextInt(100));
        } catch (InterruptedException e) {
            // intentionally empty
        }
    }


}
