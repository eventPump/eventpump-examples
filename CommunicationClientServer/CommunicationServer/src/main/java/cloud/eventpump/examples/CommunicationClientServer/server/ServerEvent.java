package cloud.eventpump.examples.CommunicationClientServer.server;

public class ServerEvent {

    private final long id;
    private final long timestampUtc;
    private final String payload;

    public ServerEvent(long id, long timestampUtc, String payload) {
        this.id = id;
        this.timestampUtc = timestampUtc;
        this.payload = payload;
    }

    public long getId() {
        return id;
    }

    public long getTimestampUtc() {
        return timestampUtc;
    }

    public String getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return "ServerEvent{" +
                "id=" + id +
                ", timestampUtc=" + timestampUtc +
                ", payload='" + payload + '\'' +
                '}';
    }
}
