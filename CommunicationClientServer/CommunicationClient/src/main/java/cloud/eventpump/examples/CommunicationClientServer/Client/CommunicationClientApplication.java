package cloud.eventpump.examples.CommunicationClientServer.Client;

import cloud.eventpump.common.EventsApplier;
import cloud.eventpump.tcpClient.TcpClientConfig;
import cloud.eventpump.tcpClient.TcpEventsPumpClient;
import cloud.eventpump.utils.marshaller.JacksonUnmarshaller;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.Random;

@SpringBootApplication
public class CommunicationClientApplication implements ApplicationRunner {

    private Random random = new Random();
    private long lastAppliedDbId = 0;

    public static void main(String[] args) {
        SpringApplication.run(CommunicationClientApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        TcpClientConfig<ClientEvent> tcpClientConfig = TcpClientConfig.create(new JacksonUnmarshaller<>(ClientEvent.class));
        tcpClientConfig.setRepeatConnectionBasicDelay(500);
        tcpClientConfig.setRepeatConnectionMaxDelay(1000);

        TcpEventsPumpClient<ClientEvent> tcpEventsPumpClient = new TcpEventsPumpClient<>(
                new EventsApplier<ClientEvent>() {
                    @Override
                    public long findLastAppliedDBId() {
                        return lastAppliedDbId;
                    }

                    @Override
                    public long applyEvents(List<ClientEvent> list) {
                        list.forEach(System.out::println);
                        randomSleep();
                        return lastAppliedDbId = list.get(list.size() - 1).getId();
                    }
                },
                tcpClientConfig
        );

        tcpEventsPumpClient.startContinuous();
    }

    private void randomSleep() {
        try {
            Thread.sleep(random.nextInt(10));
        } catch (InterruptedException e) {
            // intentionally empty
        }
    }
}
