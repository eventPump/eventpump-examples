package cloud.eventpump.examples.CommunicationClientServer.Client;

public class ClientEvent {

    private long id;
    private long timestampUtc;
    private String payload;

    public ClientEvent() {
    }


    public long getId() {
        return id;
    }

    public long getTimestampUtc() {
        return timestampUtc;
    }

    public String getPayload() {
        return payload;
    }


    @Override
    public String toString() {
        return "ClientEvent{" +
                "id=" + id +
                ", timestampUtc=" + timestampUtc +
                ", payload='" + payload + '\'' +
                '}';
    }
}
