package cloud.eventpump.examples.simpleClientServer.EPClient.eventPump;

import cloud.eventpump.common.buffers.PrimitivesReadBuffer;
import cloud.eventpump.common.marshaller.EventUnmarshaller;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EPJacksonEventUnmarshaller implements EventUnmarshaller<EPEvent> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public EPEvent unmarshall(PrimitivesReadBuffer primitivesReadBuffer) throws Exception{
        return mapper.readValue(primitivesReadBuffer.getBytes(), EPEvent.class);
    }
}
