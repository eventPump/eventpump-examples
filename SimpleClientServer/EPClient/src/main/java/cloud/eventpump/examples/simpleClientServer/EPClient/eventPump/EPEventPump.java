package cloud.eventpump.examples.simpleClientServer.EPClient.eventPump;

import cloud.eventpump.tcpClient.TcpClientConfig;
import cloud.eventpump.tcpClient.TcpEventsPumpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.InetSocketAddress;

@Component
public class EPEventPump {

    @Value("${ep.server.address}")
    private String epServerAddress;

    @Value("${ep.server.port}")
    private int epServerPort;

    @Value("${ep.client.name}")
    private String epClientName;

    private final EPEventsApplier epEventsApplier;

    public EPEventPump(EPEventsApplier epEventsApplier) {
        this.epEventsApplier = epEventsApplier;
    }

    public void start() throws IOException {
        TcpClientConfig<EPEvent> clientConfig = TcpClientConfig.create( new EPJacksonEventUnmarshaller());
        clientConfig.setServer(new InetSocketAddress(epServerAddress, epServerPort));
        clientConfig.setClientName(epClientName);
        clientConfig.setRepeatConnectionBasicDelay(1000);
        clientConfig.setRepeatConnectionMaxDelay(10000);
        TcpEventsPumpClient<EPEvent> epEventTcpEventsPumpClient = new TcpEventsPumpClient<>(epEventsApplier, clientConfig);
        epEventTcpEventsPumpClient.startContinuous();
    }

}
