package cloud.eventpump.examples.simpleClientServer.EPClient.storage;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DBClientEventRepository extends CrudRepository<DBClientEvent, Long> {

    @Query(value = "SELECT max(Id) FROM DBClient_Event", nativeQuery = true)
    Long findMaxAppliedEventId();
}
