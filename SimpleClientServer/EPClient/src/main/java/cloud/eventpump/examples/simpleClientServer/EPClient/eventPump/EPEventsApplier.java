package cloud.eventpump.examples.simpleClientServer.EPClient.eventPump;

import cloud.eventpump.common.EventsApplier;
import cloud.eventpump.examples.simpleClientServer.EPClient.storage.DBClientEvent;
import cloud.eventpump.examples.simpleClientServer.EPClient.storage.DBClientEventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.zip.CRC32;

@Component
public class EPEventsApplier implements EventsApplier<EPEvent> {

    private final Logger logger = LoggerFactory.getLogger(EPEventsApplier.class);
    private final Random random = new Random();

    private final DBClientEventRepository dbClientEventRepository;

    @Value("${ep.client.sleep.min}")
    private int epClientSleepMin;

    @Value("${ep.client.sleep.max}")
    private int epClientSleepMax;

    private long lastAppliedDBId ;

    public EPEventsApplier(DBClientEventRepository dbClientEventRepository) {
        this.dbClientEventRepository = dbClientEventRepository;
    }

    @Override
    public long findLastAppliedDBId() {
        Long dbId = dbClientEventRepository.findMaxAppliedEventId();
        lastAppliedDBId =  dbId != null ? dbId : 0 ;
        return lastAppliedDBId ;
    }

    @Override
    public long applyEvents(List<EPEvent> list) {
        long lastDBId = list.get(list.size() - 1).getId();
        logger.info("Applied events count {} last dbIs {}", list.size(), lastDBId);
        dbClientEventRepository.saveAll(convertEvents(list));
        sleep();
        return lastDBId;
    }

    private List<DBClientEvent> convertEvents(List<EPEvent> epEvents) {
        return epEvents.stream().map(this::convertEvent).collect(Collectors.toList());
    }

    private DBClientEvent convertEvent(EPEvent epEvent) {
        String localCRC = calcCRC(epEvent.getId(),
                epEvent.getCreateUtcTs(),
                epEvent.getData1(),
                epEvent.getData2(),
                epEvent.getData3());

        if ( !localCRC.equals( epEvent.getCrc())) {
            throw new RuntimeException("Incorrect CRC !!!!!") ;
        }

        if ( lastAppliedDBId +1 != epEvent.getId() ) {
            throw new RuntimeException("Incorrect dbId local=" + lastAppliedDBId + " remote=" + epEvent.getId()) ;
        }

        lastAppliedDBId ++ ;

        return new DBClientEvent(
                epEvent.getId(),
                epEvent.getCreateUtcTs(),
                epEvent.getData1(),
                epEvent.getData2(),
                epEvent.getData3());

    }

    private String calcCRC(Long id, long createUtcTs, String data1, String data2, String data3) {
        CRC32 crc32 = new CRC32();
        crc32.update(Long.toString(id).getBytes());
        crc32.update(Long.toString(createUtcTs).getBytes());
        crc32.update(data1.getBytes());
        crc32.update(data2.getBytes());
        crc32.update(data3.getBytes());
        return Long.toHexString(crc32.getValue());
    }

    private void sleep() {
        int maxDuration = epClientSleepMax - epClientSleepMin;
        if (maxDuration > 0) {
            int sleepDuration = random.nextInt(maxDuration) + epClientSleepMin;
            try {
                Thread.sleep(sleepDuration);
            } catch (InterruptedException e) {
            }
        }
    }
}
