package cloud.eventpump.examples.simpleClientServer.EPClient.storage;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DBClientEvent {
    @Id
    private Long id;
    private long createUtcTs;
    private String data1;
    private String data2;
    private String data3;
}
