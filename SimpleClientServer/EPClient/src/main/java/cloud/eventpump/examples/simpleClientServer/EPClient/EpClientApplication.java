package cloud.eventpump.examples.simpleClientServer.EPClient;

import cloud.eventpump.examples.simpleClientServer.EPClient.eventPump.EPEventPump;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpClientApplication implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(EpClientApplication.class);

    private final EPEventPump epEventPump;

    public EpClientApplication(EPEventPump epEventPump) {
        this.epEventPump = epEventPump;
    }

    public static void main(String[] args) {
        SpringApplication.run(EpClientApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        int value = 337 ;
        LOGGER.trace("doStuff needed more information - {}", value);
        LOGGER.debug("doStuff needed to debug - {}", value);
        LOGGER.info("doStuff took input - {}", value);
        LOGGER.warn("doStuff needed to warn - {}", value);
        LOGGER.error("doStuff encountered an error with value - {}", value);
        // epEventPump.start();
    }
}
