package cloud.eventpump.examples.simpleClientServer.EPClient.eventPump;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class EPEvent {
    private long id ;
    private long createUtcTs ;
    private String data1 ;
    private String data2 ;
    private String data3 ;
    private String crc ;
}
