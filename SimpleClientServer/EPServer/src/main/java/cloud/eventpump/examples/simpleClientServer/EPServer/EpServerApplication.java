package cloud.eventpump.examples.simpleClientServer.EPServer;

import cloud.eventpump.examples.simpleClientServer.EPServer.dbUpdater.DBUpdater;
import cloud.eventpump.examples.simpleClientServer.EPServer.eventPump.EPEventPump;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class EpServerApplication implements CommandLineRunner {

	private final EPEventPump epEventPump ;
	private final DBUpdater dbUpdater ;

	public EpServerApplication(EPEventPump epEventPump, DBUpdater dbUpdater) {
		this.epEventPump = epEventPump;
		this.dbUpdater = dbUpdater;
	}

	public static void main(String[] args) {
		SpringApplication.run(EpServerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		epEventPump.start();
		dbUpdater.updateDB();
	}
}
