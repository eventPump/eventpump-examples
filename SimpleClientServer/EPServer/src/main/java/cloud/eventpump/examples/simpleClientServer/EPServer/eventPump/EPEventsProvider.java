package cloud.eventpump.examples.simpleClientServer.EPServer.eventPump;

import cloud.eventpump.examples.simpleClientServer.EPServer.storage.DBMasterEvent;
import cloud.eventpump.examples.simpleClientServer.EPServer.storage.DBMasterEventRepository;
import cloud.eventpump.server.EventsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.CRC32;

@Service
public class EPEventsProvider implements EventsProvider<EPEvent> {

    private final Logger logger = LoggerFactory.getLogger(EPEventsProvider.class);

    private final DBMasterEventRepository dbMasterEventRepository;

    public EPEventsProvider(DBMasterEventRepository dbMasterEventRepository) {
        this.dbMasterEventRepository = dbMasterEventRepository;
    }

    @Override
    public List<EPEvent> provideEvents(long dbId, int maxCount) {
        List<EPEvent> collect = dbMasterEventRepository.findEvents(dbId, maxCount).stream()
                .map(this::createEPEvent)
                .collect(Collectors.toList());
        logger.info( "ProvideEvents called events count {}", collect.size());
        return collect ;
    }

    private EPEvent createEPEvent(DBMasterEvent dbMasterEvent) {
        EPEvent epEvent = new EPEvent(
                dbMasterEvent.getId(),
                dbMasterEvent.getCreateUtcTs(),
                dbMasterEvent.getData1(),
                dbMasterEvent.getData2(),
                dbMasterEvent.getData3(),
                calcCRC(
                        dbMasterEvent.getId(),
                        dbMasterEvent.getCreateUtcTs(),
                        dbMasterEvent.getData1(),
                        dbMasterEvent.getData2(),
                        dbMasterEvent.getData3()
                ));
        return epEvent;
    }

    private String calcCRC(Long id, long createUtcTs, String data1, String data2, String data3) {
        CRC32 crc32 = new CRC32();
        crc32.update(Long.toString(id).getBytes());
        crc32.update(Long.toString(createUtcTs).getBytes());
        crc32.update(data1.getBytes());
        crc32.update(data2.getBytes());
        crc32.update(data3.getBytes());
        return Long.toHexString(crc32.getValue());
    }
}
