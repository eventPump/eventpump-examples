package cloud.eventpump.examples.simpleClientServer.EPServer.storage;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DBMasterEventRepository extends CrudRepository<DBMasterEvent, Long> {

    @Query(value = "SELECT * FROM DBMaster_Event event WHERE event.id > :dbId ORDER BY event.id LIMIT :maxCount", nativeQuery = true)
    List<DBMasterEvent> findEvents(@Param("dbId") long dbId, @Param("maxCount") long maxCount);

    @Query(value = "SELECT max(Id) FROM DBMaster_Event", nativeQuery = true)
    Long findMaxEventId();
}
