package cloud.eventpump.examples.simpleClientServer.EPServer.eventPump;

import cloud.eventpump.server.EventAdapter;

public class EPAdapter implements EventAdapter<EPEvent> {
    @Override
    public long getEventId(EPEvent epEvent) {
        return epEvent.getId();
    }

    @Override
    public long getTimestampUTC(EPEvent epEvent) {
        return epEvent.getCreateUtcTs();
    }
}
