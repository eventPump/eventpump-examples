package cloud.eventpump.examples.simpleClientServer.EPServer.eventPump;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EPEvent {
    private long id ;
    private long createUtcTs ;
    private String data1 ;
    private String data2 ;
    private String data3 ;
    private String crc ;
}
