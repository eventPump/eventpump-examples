package cloud.eventpump.examples.simpleClientServer.EPServer.dbUpdater;

import cloud.eventpump.examples.simpleClientServer.EPServer.eventPump.EPEventPump;
import cloud.eventpump.examples.simpleClientServer.EPServer.storage.DBMasterEvent;
import cloud.eventpump.examples.simpleClientServer.EPServer.storage.DBMasterEventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class DBUpdater {
    private final Logger logger = LoggerFactory.getLogger(DBUpdater.class);

    @Value("${ep.updater.min.sleep.time}")
    private int minSleepTime;

    @Value("${ep.updater.max.sleep.time}")
    private int maxSleepTime;

    @Value("${ep.updater.min.transaction.event.count}")
    private int minTransactionEventCount;

    @Value("${ep.updater.max.transaction.event.count}")
    private int maxTransactionEventCount;

    @Value("${ep.updater.min.data.string.length}")
    private int minDataStringLength;

    @Value("${ep.updater.max.data.string.length}")
    private int maxDataStringLength;

    private final DBMasterEventRepository dbMasterEventRepository;
    private final EPEventPump epEventPump;

    private final Random random = new Random();
    private long dbId = -1;

    public DBUpdater(DBMasterEventRepository dbMasterEventRepository, EPEventPump epEventPump) {
        this.dbMasterEventRepository = dbMasterEventRepository;
        this.epEventPump = epEventPump;
    }

    public void updateDB() {
        setupDBId();
        for (; ; ) {
            mainLoop();
        }
    }

    private void mainLoop() {
        int count = calcRandom(minTransactionEventCount, maxTransactionEventCount);
        List<DBMasterEvent> dbMasterEvents = createEvents(count);
        dbMasterEventRepository.saveAll(dbMasterEvents);
        epEventPump.commit();
        logger.info("Added events count {} last dbId {}", dbMasterEvents.size(), dbMasterEvents.get(dbMasterEvents.size() - 1).getId());
        sleep();
    }

    private void setupDBId() {
        Long readMaxDBId = dbMasterEventRepository.findMaxEventId();
        dbId = readMaxDBId != null ? readMaxDBId + 1 : 1;
    }


    private void sleep() {
        int sleepTime = calcRandom(minSleepTime, maxSleepTime);
        if (sleepTime > 0) {
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private int calcRandom(int min, int max) {
        if (min < 0 || max < 0) {
            return 0;
        }
        int range = max - min;
        if (range > 0) {
            return min + random.nextInt(range);
        } else {
            return min;
        }
    }


    private List<DBMasterEvent> createEvents(int count) {
        List<DBMasterEvent> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            result.add(createEvent());
        }
        return result;
    }

    private DBMasterEvent createEvent() {
        System.err.println("create event dbId = " + dbId);

        return new DBMasterEvent(
                dbId++,
                System.currentTimeMillis(),
                createRandomString(),
                createRandomString(),
                createRandomString());
    }

    private String createRandomString() {
        int len = calcRandom(minDataStringLength, maxDataStringLength);
        StringBuilder stringBuilder = new StringBuilder();
        int startChr = random.nextInt(20);
        for (int i = 0; i < len; i++) {
            stringBuilder.append('a' + (i + startChr) % 20);
        }
        return stringBuilder.toString();
    }

}
