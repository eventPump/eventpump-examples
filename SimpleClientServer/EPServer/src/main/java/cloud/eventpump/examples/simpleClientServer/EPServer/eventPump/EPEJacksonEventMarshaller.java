package cloud.eventpump.examples.simpleClientServer.EPServer.eventPump;

import cloud.eventpump.common.buffers.PrimitivesWriteBuffer;
import cloud.eventpump.common.marshaller.EventMarshaller;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EPEJacksonEventMarshaller implements EventMarshaller<EPEvent> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void marshall(EPEvent epEvent, PrimitivesWriteBuffer primitivesWriteBuffer) throws Exception {
        primitivesWriteBuffer.putBytes(mapper.writeValueAsBytes(epEvent));
    }
}
