package cloud.eventpump.examples.simpleClientServer.EPServer.eventPump;

import cloud.eventpump.server.EventsProvider;
import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.ServerConfig;
import cloud.eventpump.statisticsServer.StatisticsEventPumpServer;
import cloud.eventpump.statisticsServer.StatisticsWebConfig;
import cloud.eventpump.statisticsServer.StatisticsWebEventPumpServer;
import cloud.eventpump.tcpServer.TcpEventsPumpServer;
import cloud.eventpump.tcpServer.TcpServerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.InetSocketAddress;

@Service
public class EPEventPump {

    @Value("${ep.server.address}")
    private String epServerAddress;

    @Value("${ep.server.port}")
    private int epServerPort;

    private final EventsProvider<EPEvent> epEventsProvider ;

    private EventsPumpServer<EPEvent> eventEventsPumpServer;
    private TcpEventsPumpServer<EPEvent> tcpEventsPumpServer;
    private StatisticsEventPumpServer<EPEvent> statisticsEventPumpServer;
    private StatisticsWebEventPumpServer<EPEvent> statisticsHtmlEventPumpServer;

    public EPEventPump(EventsProvider<EPEvent> epEventsProvider) {
        this.epEventsProvider = epEventsProvider;
    }

    public void start() throws IOException {
        eventEventsPumpServer = new EventsPumpServer<>(epEventsProvider, new EPAdapter(), new ServerConfig());
        TcpServerConfig<EPEvent> tcpServerConfig = new TcpServerConfig<>( new EPEJacksonEventMarshaller());
        tcpServerConfig.setEventMarshaller(new EPEJacksonEventMarshaller());
        tcpServerConfig.setServerAddress( new InetSocketAddress(epServerAddress,epServerPort));
        tcpEventsPumpServer = new TcpEventsPumpServer<>(eventEventsPumpServer, tcpServerConfig);
        statisticsEventPumpServer = new StatisticsEventPumpServer<>(eventEventsPumpServer);
        statisticsHtmlEventPumpServer = new StatisticsWebEventPumpServer<>(statisticsEventPumpServer, new StatisticsWebConfig()) ;
        tcpEventsPumpServer.start();
        statisticsHtmlEventPumpServer.start();
    }

    public void commit() {
        eventEventsPumpServer.commit();
    }

}
