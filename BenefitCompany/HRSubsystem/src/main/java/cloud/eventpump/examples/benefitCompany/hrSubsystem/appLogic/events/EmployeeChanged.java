package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.events;

import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.EmployeeId;

import java.sql.Date;

public class EmployeeChanged extends EmployeeInfo {

    public EmployeeChanged(EmployeeId employeeId, String name, String email, Address address, Address correspondenceAddress, Date dateOfBirth) {
        super("EmployeeChanged_v0100", employeeId, name, email, address, correspondenceAddress, dateOfBirth, false);
    }
}
