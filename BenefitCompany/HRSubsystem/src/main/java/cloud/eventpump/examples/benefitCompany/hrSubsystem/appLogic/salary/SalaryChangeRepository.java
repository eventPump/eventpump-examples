package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.salary;

import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.HREmployee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SalaryChangeRepository extends CrudRepository<SalaryChange, Long> {

    // List<SalaryChange> findByEmployeeAndDateOfChangeGreaterThanEqualAndDateOfChangeLessThanEqual(HREmployee employee, Date begin, Date end);
    List<SalaryChange> findByEmployee(HREmployee employee);
}
