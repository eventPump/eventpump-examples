package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrPermission;

import cloud.eventpump.examples.benefitCompany.hrSubsystem.permissionEPClient.events.PermissionType;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.EmployeeId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HRPermissionRepository extends CrudRepository<HRPermission, Long> {
    HRPermission findByEmployeeIdAndPermissionType(EmployeeId employeeId, PermissionType permissionType);

    List<HRPermission> findByEmployeeId(EmployeeId employeeId);
}
