package cloud.eventpump.examples.benefitCompany.hrSubsystem.permissionEPClient;

import cloud.eventpump.examples.benefitCompany.hrSubsystem.permissionEPClient.events.PermissionGranted;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PermissionGrantedEventHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final EmployeeService employeeService;

    public PermissionGrantedEventHandler(EmployeeService employeeService, PermissionEPClient permissionEPClient) {
        this.employeeService = employeeService;
        permissionEPClient.register("PermissionGranted_v0100", PermissionGranted.class, this::processEvent);
    }

    public void processEvent(PermissionGranted event) {
        logger.info("PermissionGranted event arrived: {}", event.toString());
        employeeService.addPermission(event.getEmployeeId(), event.getPermissionType());
    }
}
