package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic;

import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.Address;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.EmployeeId;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.HREmployee;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.HREmployeeRepository;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrPermission.HRPermission;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrPermission.HRPermissionRepository;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.salary.SalaryChange;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.salary.SalaryChangeRepository;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.salary.SalaryPayment;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.salary.SalaryPaymentRepository;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.hrEmployeeEPServer.HREmployeeEPServer;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.permissionEPClient.events.PermissionType;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.events.EmployeeAdded;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.events.EmployeeChanged;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.StringJoiner;

@Service
public class EmployeeService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private HREmployeeRepository hrEmployeeRepository;

    @Autowired
    private SalaryPaymentRepository salaryPaymentRepository;

    @Autowired
    private SalaryChangeRepository salaryChangeRepository;

    @Autowired
    private HRPermissionRepository hrPermissionRepository;

    @Autowired
    private HREmployeeEPServer hrEmployeeEPServer ;

    // Only for init purposes
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createRoot(String name, String email, Address address, Address correspondenceAddress, BigInteger salary, Date dateOfBirth) {
        logger.info("createRoot : {}", name);
        HREmployee root = new HREmployee(EmployeeId.create(), name, email, address, correspondenceAddress, salary, dateOfBirth, true, null);
        root.setCreatedBy(root);
        hrEmployeeRepository.save(root);
        show("createRoot");
        hrEmployeeEPServer.handleEvent(new EmployeeAdded(
                root.getEmployeeId(),
                root.getName(),
                root.getEmail(),
                convertAddress(root.getAddress()),
                convertAddress(root.getCorrespondenceAddress()),
                root.getDateOfBirth(),
                true));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void addEmployee(String name, String email, Address address, Address correspondenceAddress, BigInteger salary, Date dateOfBirth, String createdByName) {
        logger.info("addEmployee : {}", name);
        HREmployee createdBy = getEmployeeByName(createdByName);
        checkPermission(createdBy, PermissionType.ManageEmployees);
        HREmployee hrEmployee = new HREmployee(EmployeeId.create(), name, email, address, correspondenceAddress, salary, dateOfBirth, createdBy);
        hrEmployeeRepository.save(hrEmployee);
        show("addEmployee");
        hrEmployeeEPServer.handleEvent(new EmployeeAdded(
                hrEmployee.getEmployeeId(),
                hrEmployee.getName(),
                hrEmployee.getEmail(),
                convertAddress(hrEmployee.getAddress()),
                convertAddress(hrEmployee.getCorrespondenceAddress()),
                hrEmployee.getDateOfBirth(),
                false));
    }


    // TODO Employee change history
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void changeEmployeeData(String employeeName, String email, Address address, Address correspondenceAddress, Date dateOfBirth, String changedByName) {
        logger.info("changeEmployeeData : {}", employeeName);
        HREmployee changedBy = getEmployeeByName(changedByName);
        checkPermission(changedBy, PermissionType.ManageEmployees);
        HREmployee employee = getEmployeeByName(employeeName);
        if (email != null) {
            employee.setEmail(email);
        }
        if (address != null) {
            employee.setAddress(address);
        }
        if (correspondenceAddress != null) {
            employee.setCorrespondenceAddress(correspondenceAddress);
        }
        if (dateOfBirth != null) {
            employee.setDateOfBirth(dateOfBirth);
        }
        show("changeEmployee");
        hrEmployeeEPServer.handleEvent(new EmployeeChanged(
                employee.getEmployeeId(),
                employee.getName(),
                employee.getEmail(),
                convertAddress(employee.getAddress()),
                convertAddress(employee.getCorrespondenceAddress()),
                employee.getDateOfBirth()));

    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void changeSalary(String employeeName, BigInteger salary, String changedByEmployeeName) {
        logger.info("changeSalary : {}", employeeName);
        HREmployee changedByEmployee = getEmployeeByName(changedByEmployeeName);
        checkPermission(changedByEmployee, PermissionType.ManageSalaries);
        HREmployee employee = getEmployeeByName(employeeName);
        Date date = getCurrentDate();
        SalaryChange salaryChange = new SalaryChange(employee.getSalary(), date, "", employee, changedByEmployee);
        salaryChangeRepository.save(salaryChange);
        employee.setSalary(salary);
        show("changeSalary");
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void paySalary(String salaryForEmployeeName, String transferredByEmployeeName) {
        logger.info("paySalary : {}", salaryForEmployeeName);
        HREmployee transferredByEmployee = getEmployeeByName(transferredByEmployeeName);
        checkPermission(transferredByEmployee, PermissionType.ManageSalaries);
        HREmployee salaryForEmployee = getEmployeeByName(salaryForEmployeeName);
        Date date = getCurrentDate();
        SalaryPayment salaryPayment = new SalaryPayment(salaryForEmployee.getSalary(), date, "", salaryForEmployee, transferredByEmployee);
        salaryPaymentRepository.save(salaryPayment);
        show("paySalary");
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void addPermission(EmployeeId employeeId, PermissionType permissionType) {
        logger.info("addPermission : {}", employeeId);
        HRPermission hrPermission = hrPermissionRepository.findByEmployeeIdAndPermissionType(employeeId, permissionType);
        if (hrPermission == null) {
            hrPermissionRepository.save(new HRPermission(employeeId, permissionType));
            show("addPermission");
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void revokePermission(EmployeeId employeeId, PermissionType permissionType) {
        logger.info("revokePermission : {}", employeeId);
        HRPermission hrPermission = hrPermissionRepository.findByEmployeeIdAndPermissionType(employeeId, permissionType);
        if (hrPermission != null) {
            hrPermissionRepository.delete(hrPermission);
        }
        show("revokePermission");
    }

    public List<SalaryChange> getSalaryChanges(HREmployee employee, Date begin, Date end) {
        return salaryChangeRepository.findByEmployee(employee);
    }

    public List<SalaryPayment> getSalaryPayments(HREmployee employee, Date begin, Date end) {
        return salaryPaymentRepository.findBySalaryFor(employee);
    }

    public void checkPermission(HREmployee employee, PermissionType permissionType) {
        if (employee.isRoot() || null != hrPermissionRepository.findByEmployeeIdAndPermissionType(employee.getEmployeeId(), permissionType)) {
            logger.info("Employee : {} has permission : {}", employee.getName(), permissionType.toString());
        } else {
            throw new RuntimeException("Employee : " + employee.getName() + " not authorized : " + permissionType.toString());
        }
    }

    public boolean hasPermission(String employeeName, PermissionType permissionType) {
        HREmployee employee = getEmployeeByName(employeeName);
        return null != hrPermissionRepository.findByEmployeeIdAndPermissionType(employee.getEmployeeId(), permissionType);
    }

    private HREmployee getEmployeeByName(String name) {
        return hrEmployeeRepository.findByName(name).orElseThrow(
                () -> new RuntimeException("Employee not found name=" + name));
    }

    private Date getCurrentDate() {
        return Date.valueOf(LocalDate.now());
    }

    private cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.events.Address convertAddress(Address address) {
        return new cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.events.Address(address.getFirstLine(), address.getSecondLine(), address.getThreadLine());
    }

    private void show(String action) {
        StringBuilder message = new StringBuilder();
        for (HREmployee hrEmployee : hrEmployeeRepository.findAll()) {
            message.append("\n\t" + hrEmployee.getName() + " " + hrEmployee.getEmail() + " " + hrEmployee.getDateOfBirth() + " " + hrEmployee.getSalary() + " " + hrEmployee.getEmployeeId());

            StringJoiner stringJoiner = new StringJoiner(", ");
            getSalaryPayments(hrEmployee, new Date(0), new Date(Integer.MAX_VALUE)).forEach(sp ->
                    stringJoiner.add("{" + sp.getSalaryPayed() + "," + sp.getDateOfPayment() + "}"));
            if (stringJoiner.length() > 0) {
                message.append("\n\t\t salary payed: " + stringJoiner);
            }

            StringJoiner stringJoiner1 = new StringJoiner(", ");
            getSalaryChanges(hrEmployee, new Date(0), new Date(Integer.MAX_VALUE)).forEach(sc ->
                    stringJoiner1.add("{" + sc.getPreviousSalary() + "," + sc.getDateOfChange() + "}"));
            if (stringJoiner1.length() > 0) {
                message.append("\n\t\t salary changed: " + stringJoiner1);
            }

            StringJoiner stringJoiner2 = new StringJoiner(", ");

            List<HRPermission> byEmployeeId = hrPermissionRepository.findByEmployeeId(hrEmployee.getEmployeeId());

            hrPermissionRepository.findByEmployeeId(hrEmployee.getEmployeeId()).forEach(a ->
                    stringJoiner2.add(a.getPermissionType().toString()));
            if (stringJoiner2.length() > 0) {
                message.append("\n\t\t permissions: " + stringJoiner2);
            }
        }
        logger.info("HRDatabase after action: {} {}", action, message);
    }

}
