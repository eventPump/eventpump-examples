package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.salary;

import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.HREmployee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SalaryPaymentRepository extends CrudRepository<SalaryPayment, Long> {

    // List<SalaryPayment> findBySalaryForAndDateOfPaymentGreaterThanEqualAndDateOfPaymentLessThanEqual(HREmployee employee, Date begin, Date end);

    List<SalaryPayment> findBySalaryFor(HREmployee employee);
}
