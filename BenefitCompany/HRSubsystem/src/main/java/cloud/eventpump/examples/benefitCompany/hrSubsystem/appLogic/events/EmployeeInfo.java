package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.events;

import cloud.eventpump.examples.benefitCompany.EventPump.server.ServerEvent;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.EmployeeId;

import java.sql.Date;


public class EmployeeInfo extends ServerEvent {
    private EmployeeId employeeId;
    private String name;
    private String email;
    private Address address;
    private Address correspondenceAddress;
    private Date dateOfBirth;
    private boolean root;

    public EmployeeInfo(String serverEventType, EmployeeId employeeId, String name, String email, Address address, Address correspondenceAddress, Date dateOfBirth, boolean root) {
        super(serverEventType);
        this.employeeId = employeeId;
        this.name = name;
        this.email = email;
        this.address = address;
        this.correspondenceAddress = correspondenceAddress;
        this.dateOfBirth = dateOfBirth;
        this.root = root;
    }

    public EmployeeId getEmployeeId() {
        return employeeId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Address getAddress() {
        return address;
    }

    public Address getCorrespondenceAddress() {
        return correspondenceAddress;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public boolean isRoot() {
        return root;
    }

    @Override
    public String toString() {
        return "EmployeeInfo{" +
                "employeeId=" + employeeId +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", address=" + address +
                ", correspondenceAddress=" + correspondenceAddress +
                ", dateOfBirth=" + dateOfBirth +
                ", isRoot=" + root +
                '}';
    }
}
