package cloud.eventpump.examples.benefitCompany.hrSubsystem.permissionEPClient;

import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.EmployeeService;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.permissionEPClient.events.PermissionRevoked;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PermissionRevokedEventHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final EmployeeService employeeService;

    public PermissionRevokedEventHandler(EmployeeService employeeService, PermissionEPClient permissionEPClient) {
        this.employeeService = employeeService;
        permissionEPClient.register("PermissionRevoked_v0100", PermissionRevoked.class, this::processEvent);
    }

    private void processEvent(PermissionRevoked event) {
        logger.info("PermissionRevoked event arrived: {}", event.toString());
        employeeService.revokePermission(event.getEmployeeId(), event.getPermissionType());
    }
}
