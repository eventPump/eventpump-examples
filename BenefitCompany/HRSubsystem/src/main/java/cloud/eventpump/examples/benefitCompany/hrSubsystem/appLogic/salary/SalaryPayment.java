package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.salary;

import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.HREmployee;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Date;

@Entity
@Data
@NoArgsConstructor
@ToString
public class SalaryPayment {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private BigInteger salaryPayed ;
    private Date dateOfPayment ;
    private String comment ;

    @ManyToOne(fetch= FetchType.EAGER)
    private HREmployee salaryFor;

    @ManyToOne(fetch= FetchType.EAGER)
    private HREmployee transferredBy;

    public SalaryPayment(BigInteger salaryPayed, Date dateOfPayment, String comment, HREmployee salaryFor, HREmployee transferredBy) {
        this.salaryPayed = salaryPayed;
        this.dateOfPayment = dateOfPayment;
        this.comment = comment;
        this.salaryFor = salaryFor;
        this.transferredBy = transferredBy;
    }
}
