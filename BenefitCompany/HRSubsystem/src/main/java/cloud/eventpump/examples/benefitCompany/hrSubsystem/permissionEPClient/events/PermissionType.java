package cloud.eventpump.examples.benefitCompany.hrSubsystem.permissionEPClient.events;

public enum PermissionType {
    // TODO should be removed from client,or errors properly handled
    ManageEmployees, ManageRights, ManageBenefits, ManageSalaries
}

