package cloud.eventpump.examples.benefitCompany.hrSubsystem;

import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.EmployeeService;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.Address;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.permissionEPClient.events.PermissionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.math.BigInteger;
import java.sql.Date;

@SpringBootApplication
@ComponentScan(basePackages = {
        "cloud.eventpump.examples.benefitCompany.hrSubsystem",
        "cloud.eventpump.examples.benefitCompany.EventPump"})
@EnableJpaRepositories(basePackages = {
        "cloud.eventpump.examples.benefitCompany.hrSubsystem",
        "cloud.eventpump.examples.benefitCompany.EventPump"})
@EntityScan(basePackages = {
        "cloud.eventpump.examples.benefitCompany.hrSubsystem",
        "cloud.eventpump.examples.benefitCompany.EventPump"})
public class HRApplication implements CommandLineRunner {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private EmployeeService employeeService;

    public static void main(String[] args) {
        SpringApplication.run(HRApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        employeeService.createRoot("root", "root@email.com",
                new Address("", "", ""),
                new Address("", "", ""),
                new BigInteger("0"), Date.valueOf("1900-01-01"));

        addEmployee("HR-Manager", new BigInteger("10000"), "HR-Manager@hr.com", "root");
        addEmployee("IT-Manager", new BigInteger("5000"), "IT-Manager@it.com", "root");

        waitForPermission("HR-Manager", PermissionType.ManageEmployees);

        addEmployee("hr1", new BigInteger("1500"), "hr1@hr.com", "HR-Manager");
        addEmployee("hr2", new BigInteger("1100"), "hr2@hr.com", "HR-Manager");

        addEmployee("dev1", new BigInteger("1100"), "dev1@hr.com", "HR-Manager");
        addEmployee("dev2", new BigInteger("1200"), "dev2@hr.com", "HR-Manager");
        addEmployee("dev3", new BigInteger("1300"), "dev3@hr.com", "HR-Manager");

        waitForPermission("hr1", PermissionType.ManageSalaries);

        employeeService.paySalary("dev1", "hr1");
        employeeService.paySalary("dev2", "hr1");
        try {
            employeeService.paySalary("dev3", "hr2");
        } catch (RuntimeException ex) {
            // expected exception hr2 does not have authorization to transfer salary
        }

        waitForPermission("dev1", PermissionType.ManageEmployees);
    }

    private void waitForPermission(String employeeName, PermissionType permissionType) throws InterruptedException {
        logger.info("Waiting for authorization : {} for employee : {} ...", permissionType, employeeName);
        while (!employeeService.hasPermission(employeeName, permissionType)) {
            Thread.sleep(100);
        }
    }

    private void addEmployee(String name, BigInteger salary, String email, String createdBy) {
        employeeService.addEmployee(name, email,
                new Address(name + "-a-l1", name + "-a-l2", name + "-a-l3"),
                new Address(name + "-c-l1", name + "-c-l2", name + "-c-l3"),
                salary, Date.valueOf("1980-01-01"), createdBy);
    }

}
