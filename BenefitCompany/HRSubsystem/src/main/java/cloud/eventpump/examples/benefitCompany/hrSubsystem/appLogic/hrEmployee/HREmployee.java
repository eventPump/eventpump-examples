package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Date;

@Entity
@Data
@NoArgsConstructor
@ToString
public class HREmployee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Embedded
    private EmployeeId employeeId;
    private String name;
    private String email;
    @AttributeOverrides({
            @AttributeOverride(name = "firstLine", column = @Column(name = "addressFirstLine")),
            @AttributeOverride(name = "secondLine", column = @Column(name = "addressSecondLine")),
            @AttributeOverride(name = "threadLine", column = @Column(name = "addressThreadLine"))
    })
    @Embedded
    private Address address;
    @AttributeOverrides({
            @AttributeOverride(name = "firstLine", column = @Column(name = "correspondenceAddressFirstLine")),
            @AttributeOverride(name = "secondLine", column = @Column(name = "correspondenceAddressSecondLine")),
            @AttributeOverride(name = "threadLine", column = @Column(name = "correspondenceAddressThreadLine"))
    })
    @Embedded
    private Address correspondenceAddress;
    private BigInteger salary;
    private Date dateOfBirth;
    private boolean isRoot;
    @ManyToOne
    private HREmployee createdBy;

    public HREmployee(EmployeeId employeeId, String name, String email, Address address, Address correspondenceAddress, BigInteger salary, Date dateOfBirth, HREmployee createdBy) {
        this(employeeId, name, email, address, correspondenceAddress, salary, dateOfBirth, false, createdBy);
    }

    public HREmployee(EmployeeId employeeId, String name, String email, Address address, Address correspondenceAddress, BigInteger salary, Date dateOfBirth, boolean isRoot, HREmployee cratedBy) {
        this.employeeId = employeeId;
        this.name = name;
        this.email = email;
        this.address = address;
        this.correspondenceAddress = correspondenceAddress;
        this.salary = salary;
        this.dateOfBirth = dateOfBirth;
        this.isRoot = isRoot;
        this.createdBy = cratedBy;
    }
}
