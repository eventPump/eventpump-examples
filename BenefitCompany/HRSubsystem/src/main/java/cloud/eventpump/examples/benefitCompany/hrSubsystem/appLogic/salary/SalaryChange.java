package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.salary;

import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.HREmployee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SalaryChange {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private BigInteger previousSalary ;
    private Date dateOfChange ;
    private String comment ;

    @ManyToOne(fetch= FetchType.EAGER)
    private HREmployee employee;

    @ManyToOne(fetch= FetchType.EAGER)
    private HREmployee changedBy;

    public SalaryChange(BigInteger previousSalary, Date dateOfChange, String comment, HREmployee employee, HREmployee changedBy) {
        this.previousSalary = previousSalary;
        this.dateOfChange = dateOfChange;
        this.comment = comment;
        this.employee = employee;
        this.changedBy = changedBy;
    }
}
