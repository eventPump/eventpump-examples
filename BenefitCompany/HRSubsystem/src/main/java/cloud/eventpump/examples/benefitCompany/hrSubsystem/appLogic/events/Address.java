package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.events;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class Address {

    String firstLine;
    String secondLine;
    String threadLine;

}
