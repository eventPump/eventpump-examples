package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee;

import lombok.*;

import javax.persistence.Embeddable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Embeddable
public class Address {

    String firstLine ;
    String secondLine ;
    String threadLine ;

}
