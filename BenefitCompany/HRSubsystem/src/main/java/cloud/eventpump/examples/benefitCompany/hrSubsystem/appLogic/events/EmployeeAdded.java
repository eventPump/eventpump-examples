package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.events;

import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.EmployeeId;

import java.sql.Date;


public class EmployeeAdded extends EmployeeInfo {

    public EmployeeAdded(EmployeeId employeeId, String name, String email, Address address, Address correspondenceAddress, Date dateOfBirth, boolean isRoot) {
        super("EmployeeAdded_v0100", employeeId, name, email, address, correspondenceAddress, dateOfBirth, isRoot);
    }
}
