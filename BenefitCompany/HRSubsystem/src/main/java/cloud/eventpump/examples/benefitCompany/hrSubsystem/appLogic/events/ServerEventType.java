package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.events;

public enum ServerEventType {
    EmployeeAdded_v0100,
    EmployeeChanged_v0100
}
