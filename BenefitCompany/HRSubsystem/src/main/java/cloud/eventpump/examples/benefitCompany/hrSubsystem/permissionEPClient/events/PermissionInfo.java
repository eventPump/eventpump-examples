package cloud.eventpump.examples.benefitCompany.hrSubsystem.permissionEPClient.events;


import cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler.ClientEvent;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.EmployeeId;


public class PermissionInfo extends ClientEvent {

    private EmployeeId employeeId;
    private PermissionType permissionType;

    public EmployeeId getEmployeeId() {
        return employeeId;
    }

    public PermissionType getPermissionType() {
        return permissionType;
    }

    @Override
    public String toString() {
        return "PermissionInfo{" +
                "employeeId=" + employeeId +
                ", permissionType=" + permissionType +
                '}';
    }
}
