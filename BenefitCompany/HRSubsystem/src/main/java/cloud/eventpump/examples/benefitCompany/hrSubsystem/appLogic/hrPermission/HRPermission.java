package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrPermission;

import cloud.eventpump.examples.benefitCompany.hrSubsystem.permissionEPClient.events.PermissionType;
import cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee.EmployeeId;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@ToString
public class HRPermission {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Embedded
    private EmployeeId employeeId ;
    private PermissionType permissionType;

    public HRPermission(EmployeeId employeeId, PermissionType permissionType) {
        this.employeeId = employeeId;
        this.permissionType = permissionType;
    }
}
