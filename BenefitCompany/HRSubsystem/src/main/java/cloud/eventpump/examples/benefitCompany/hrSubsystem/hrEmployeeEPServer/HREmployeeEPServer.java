package cloud.eventpump.examples.benefitCompany.hrSubsystem.hrEmployeeEPServer;

import cloud.eventpump.examples.benefitCompany.EventPump.genericServerSpring.CommitHandler;
import cloud.eventpump.examples.benefitCompany.EventPump.server.EPServer;
import cloud.eventpump.examples.benefitCompany.EventPump.server.EventHandler;
import cloud.eventpump.examples.benefitCompany.EventPump.server.ServerEvent;
import cloud.eventpump.examples.benefitCompany.EventPump.genericServerSpring.AEventStore;
import cloud.eventpump.examples.benefitCompany.EventPump.genericServerSpring.AServerCommitInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
public class HREmployeeEPServer implements EventHandler, CommitHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${ep.hrEmployee.server.address}")
    private String epServerAddress;

    @Value("${ep.hrEmployee.server.port}")
    private int epServerPort;

    @Value("${ep.hrEmployee.statisticsServer.port}")
    private int epStatisticsServerPort;

    @Autowired
    private AEventStore eventStore;

    private EPServer epServer;

    @PostConstruct
    public void start() throws IOException {
        AServerCommitInterceptor.setCommitHandler(this);
        epServer = new EPServer(epServerAddress, epServerPort, epStatisticsServerPort, eventStore);
        epServer.start();
    }

    @Override
    public void handleEvent(ServerEvent serverEvent) {
        logger.info("Event raised : {}", serverEvent.toString());
        epServer.handleEvent(serverEvent);
    }

    public void commit() {
        logger.info("commit has happened");
        epServer.commit();
    }
}
