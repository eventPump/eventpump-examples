package cloud.eventpump.examples.benefitCompany.hrSubsystem.appLogic.hrEmployee;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HREmployeeRepository extends CrudRepository<HREmployee, Long> {

    Optional<HREmployee> findByName(String name);
}
