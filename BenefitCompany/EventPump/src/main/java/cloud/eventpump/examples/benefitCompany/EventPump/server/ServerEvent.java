package cloud.eventpump.examples.benefitCompany.EventPump.server;

public class ServerEvent {

    private final String eventType ;

    public ServerEvent(String eventType) {
        this.eventType = eventType;
    }

    public String getEventType() {
        return eventType;
    }
}
