package cloud.eventpump.examples.benefitCompany.EventPump.genericServerSpring;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AServerEventRepository extends CrudRepository<AServerEvent,Long> {

    @Query(value = "SELECT * FROM AServer_Event event WHERE event.id > :dbId ORDER BY event.id LIMIT :maxCount", nativeQuery = true)
    List<AServerEvent> findEvents(@Param("dbId") long dbId, @Param("maxCount") long maxCount);

}
