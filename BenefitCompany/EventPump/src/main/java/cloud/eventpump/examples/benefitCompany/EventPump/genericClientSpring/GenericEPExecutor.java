package cloud.eventpump.examples.benefitCompany.EventPump.genericClientSpring;

import cloud.eventpump.examples.benefitCompany.EventPump.client.EPExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
public class GenericEPExecutor implements EPExecutor {

    @Autowired
    private GenericAppliedIdRepository genericAppliedIdRepository;

    @Override
    public long findLastAppliedDBId(long appliedIdId) {
        return genericAppliedIdRepository
                .findById(appliedIdId)
                .map(GenericAppliedId::getAppliedId)
                .orElse(0L);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void applyExecutorAndId(Runnable eventExecutor, long appliedIdId, long id) {
        eventExecutor.run();
        updateAppliedEventId(appliedIdId, id);
    }

    private void updateAppliedEventId(long appliedIdId, long id) {
        Optional<GenericAppliedId> byId = genericAppliedIdRepository.findById(appliedIdId);
        if (byId.isPresent()) {
            byId.get().setAppliedId(id);
        } else {
            genericAppliedIdRepository.save(new GenericAppliedId(appliedIdId, id));
        }
    }
}
