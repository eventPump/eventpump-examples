package cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler;

public abstract class ClientEvent {
    private long id = -1;
    private long createUtcTs = -1;

    public void setHeaderValues(long id, long createUtcTs) {
        this.id = id;
        this.createUtcTs = createUtcTs;
    }

    public long getId() {
        return id;
    }

    public long getCreateUtcTs() {
        return createUtcTs;
    }
}
