package cloud.eventpump.examples.benefitCompany.EventPump.genericServerSpring;

import cloud.eventpump.examples.benefitCompany.EventPump.server.GenericEventStore;
import cloud.eventpump.examples.benefitCompany.EventPump.server.GenericServerEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AEventStore implements GenericEventStore {

    @Autowired
    private AServerEventRepository serverEventRepository;

    @Override
    public void store(String eventType, long currentTimeMillis, String payload) {
        serverEventRepository.save(new AServerEvent(currentTimeMillis, eventType, payload));
    }

    @Override
    public List<GenericServerEvent> provideEvents(long dbId, int maxCount) {
        System.out.println( "AEventStore provideEvents dbId: " + dbId + " maxCount: " + maxCount );
        List<GenericServerEvent> genericServerEvents = new ArrayList<>();
        List<AServerEvent> events = serverEventRepository.findEvents(dbId, maxCount);
        if (events != null) {
            for (AServerEvent event : events) {
                genericServerEvents.add(new GenericServerEvent(
                        event.getId(),
                        event.getCreateUtcTs(),
                        event.getServerEventType(),
                        event.getPayload()));
            }
        }
        System.out.println( "AEventStore provideEvents dbId: " + dbId + " maxCount: " + maxCount + " returned count: " + genericServerEvents.size());
        return genericServerEvents;
    }
}
