package cloud.eventpump.examples.benefitCompany.EventPump.server;

public class GenericServerEvent {

    private long id ;
    private long createUtcTs;
    private String serverEventType;
    private String payload;

    public GenericServerEvent(long id, long createUtcTs, String serverEventType, String payload) {
        this.id = id;
        this.createUtcTs = createUtcTs;
        this.serverEventType = serverEventType;
        this.payload = payload;
    }

    public long getId() {
        return id;
    }

    public long getCreateUtcTs() {
        return createUtcTs;
    }

    public String getServerEventType() {
        return serverEventType;
    }

    public String getPayload() {
        return payload;
    }
}
