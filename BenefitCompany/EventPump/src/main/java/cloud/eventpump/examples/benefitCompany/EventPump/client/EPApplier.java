package cloud.eventpump.examples.benefitCompany.EventPump.client;

import cloud.eventpump.common.EventsApplier;
import cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler.EventHandlerHost;
import cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler.GenericClientEvent;

import java.util.List;

public class EPApplier implements EventsApplier<GenericClientEvent> {

    private final EventHandlerHost eventHandlerHost;
    private final long appliedIdId;
    private final cloud.eventpump.examples.benefitCompany.EventPump.client.EPExecutor EPExecutor;

    public EPApplier(EventHandlerHost eventHandlerHost, long appliedIdId, EPExecutor EPExecutor) {
        this.eventHandlerHost = eventHandlerHost;
        this.appliedIdId = appliedIdId;
        this.EPExecutor = EPExecutor;
    }

    @Override
    public long findLastAppliedDBId() {
        return EPExecutor.findLastAppliedDBId(appliedIdId);
    }

    @Override
    public long applyEvents(List<GenericClientEvent> genericClientEvents) {
        for (GenericClientEvent genericClientEvent : genericClientEvents) {
            EPExecutor.applyExecutorAndId(
                    () -> eventHandlerHost.process(genericClientEvent),
                    appliedIdId, genericClientEvent.getId());
        }
        int lastIdx = genericClientEvents.size() - 1;
        return lastIdx >= 0 ? genericClientEvents.get(lastIdx).getId() : -1L;
    }

}
