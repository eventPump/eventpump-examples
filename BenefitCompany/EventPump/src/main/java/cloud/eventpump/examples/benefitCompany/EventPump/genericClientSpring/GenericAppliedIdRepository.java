package cloud.eventpump.examples.benefitCompany.EventPump.genericClientSpring;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenericAppliedIdRepository extends CrudRepository<GenericAppliedId, Long> {
}
