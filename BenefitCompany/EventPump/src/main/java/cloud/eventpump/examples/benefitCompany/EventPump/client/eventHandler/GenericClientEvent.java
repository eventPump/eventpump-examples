package cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler;

public class GenericClientEvent {

    private final long id;
    private final long createUtcTs;
    private final String eventType;
    private final String payload;

    public GenericClientEvent(long id, long createUtcTs, String eventType, String payload) {
        this.id = id;
        this.createUtcTs = createUtcTs;
        this.eventType = eventType;
        this.payload = payload;
    }

    public long getId() {
        return id;
    }

    public long getCreateUtcTs() {
        return createUtcTs;
    }

    public String getEventType() {
        return eventType;
    }

    public String getPayload() {
        return payload;
    }
}
