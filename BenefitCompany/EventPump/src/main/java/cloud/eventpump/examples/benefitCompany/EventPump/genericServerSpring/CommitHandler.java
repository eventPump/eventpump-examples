package cloud.eventpump.examples.benefitCompany.EventPump.genericServerSpring;

public interface CommitHandler {

    void commit();
}
