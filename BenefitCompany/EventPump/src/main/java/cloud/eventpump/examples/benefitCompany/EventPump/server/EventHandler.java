package cloud.eventpump.examples.benefitCompany.EventPump.server;

public interface EventHandler {

    void handleEvent(ServerEvent event);

}
