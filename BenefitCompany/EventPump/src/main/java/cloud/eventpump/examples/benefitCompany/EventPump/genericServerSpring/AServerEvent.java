package cloud.eventpump.examples.benefitCompany.EventPump.genericServerSpring;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@ToString
public class AServerEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private long createUtcTs;
    private String serverEventType;
    @Column(length = 10240)
    private String payload;

    public AServerEvent(long createUtcTs, String serverEventType, String payload) {
        this.createUtcTs = createUtcTs;
        this.serverEventType = serverEventType;
        this.payload = payload;
    }
}
