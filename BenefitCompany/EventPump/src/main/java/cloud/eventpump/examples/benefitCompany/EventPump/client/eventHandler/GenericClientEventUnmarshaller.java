package cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler;

import cloud.eventpump.common.buffers.PrimitivesReadBuffer;
import cloud.eventpump.common.marshaller.EventUnmarshaller;

public class GenericClientEventUnmarshaller implements EventUnmarshaller<GenericClientEvent> {
    @Override
    public GenericClientEvent unmarshall(PrimitivesReadBuffer primitivesReadBuffer) throws Exception {
        long eventId = primitivesReadBuffer.getLong() ;
        long createUtcTs = primitivesReadBuffer.getLong() ;
        String eventType = primitivesReadBuffer.getString() ;
        String payload = primitivesReadBuffer.getString() ;
        return new GenericClientEvent( eventId, createUtcTs, eventType, payload ) ;
    }
}
