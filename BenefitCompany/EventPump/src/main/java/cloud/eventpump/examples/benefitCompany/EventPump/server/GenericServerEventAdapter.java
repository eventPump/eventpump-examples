package cloud.eventpump.examples.benefitCompany.EventPump.server;


import cloud.eventpump.server.EventAdapter;

class GenericServerEventAdapter implements EventAdapter<GenericServerEvent> {
    @Override
    public long getEventId(GenericServerEvent epEvent) {
        return epEvent.getId();
    }

    @Override
    public long getTimestampUTC(GenericServerEvent epEvent) {
        return epEvent.getCreateUtcTs();
    }
}
