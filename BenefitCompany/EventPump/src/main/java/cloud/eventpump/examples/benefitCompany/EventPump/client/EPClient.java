package cloud.eventpump.examples.benefitCompany.EventPump.client;

import cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler.*;
import cloud.eventpump.tcpClient.TcpClientConfig;
import cloud.eventpump.tcpClient.TcpEventsPumpClient;

import java.net.InetSocketAddress;

public class EPClient {

    private final EventHandlerHost eventHandlerHost = new EventHandlerHost();

    public <Event extends ClientEvent> void register(String eventType, Class<Event> eventClass, EventHandler<Event> eventEventHandler) {
        eventHandlerHost.register(eventType, eventClass, eventEventHandler);
    }

    public void start(EPExecutor epExecutor, long appliedIdId, String epServerAddress, int epServerPort, String epClientName) {
        TcpClientConfig<GenericClientEvent> clientConfig = TcpClientConfig.create(new GenericClientEventUnmarshaller());
        clientConfig.setEventUnmarshaller(new GenericClientEventUnmarshaller());
        clientConfig.setServer(new InetSocketAddress(epServerAddress, epServerPort));
        clientConfig.setClientName(epClientName);
        clientConfig.setRepeatConnectionBasicDelay(100);
        clientConfig.setRepeatConnectionMaxDelay(3000);

        EPApplier epApplier = new EPApplier(eventHandlerHost, appliedIdId, epExecutor);
        TcpEventsPumpClient<GenericClientEvent> epEventTcpEventsPumpClient = new TcpEventsPumpClient<>(epApplier, clientConfig);
        epEventTcpEventsPumpClient.startContinuous();
    }

}
