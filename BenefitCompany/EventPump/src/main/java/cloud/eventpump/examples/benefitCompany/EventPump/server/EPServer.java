package cloud.eventpump.examples.benefitCompany.EventPump.server;

import cloud.eventpump.server.EventsProvider;
import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.ServerConfig;
import cloud.eventpump.statisticsServer.StatisticsEventPumpServer;
import cloud.eventpump.statisticsServer.StatisticsWebConfig;
import cloud.eventpump.statisticsServer.StatisticsWebEventPumpServer;
import cloud.eventpump.tcpServer.TcpEventsPumpServer;
import cloud.eventpump.tcpServer.TcpServerConfig;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;


public class EPServer {

    private final ObjectMapper mapper = new ObjectMapper();

    private final String epServerAddress;
    private final int epServerPort;
    private final int epStatisticsServerPort;
    private final GenericEventStore genericEventStore;

    private EventsPumpServer<GenericServerEvent> eventEventsPumpServer;
    private TcpEventsPumpServer<GenericServerEvent> tcpEventsPumpServer;
    private StatisticsEventPumpServer<GenericServerEvent> statisticsEventPumpServer;
    private StatisticsWebEventPumpServer<GenericServerEvent> statisticsHtmlEventPumpServer;

    public EPServer(String epServerAddress, int epServerPort, int epStatisticsServerPort, GenericEventStore genericEventStore) {
        this.epServerAddress = epServerAddress;
        this.epServerPort = epServerPort;
        this.epStatisticsServerPort = epStatisticsServerPort;
        this.genericEventStore = genericEventStore;
    }

    public synchronized void start() throws IOException {
        eventEventsPumpServer = new EventsPumpServer<>(new EventsProvider<GenericServerEvent>() {
            @Override
            public List<GenericServerEvent> provideEvents(long dbId, int maxCount) {
                return genericEventStore.provideEvents(dbId, maxCount);
            }
        }, new GenericServerEventAdapter(), new ServerConfig());
        TcpServerConfig<GenericServerEvent> tcpServerConfig = new TcpServerConfig<>(new GenericServerEventMarshaller());
        tcpServerConfig.setServerAddress(new InetSocketAddress(epServerAddress, epServerPort));
        tcpEventsPumpServer = new TcpEventsPumpServer<>(eventEventsPumpServer, tcpServerConfig);
        statisticsEventPumpServer = new StatisticsEventPumpServer<>(eventEventsPumpServer);
        StatisticsWebConfig statisticsWebConfig = new StatisticsWebConfig();
        statisticsWebConfig.setPort(epStatisticsServerPort);
        statisticsHtmlEventPumpServer = new StatisticsWebEventPumpServer<>(statisticsEventPumpServer, statisticsWebConfig);
        tcpEventsPumpServer.start();
        statisticsHtmlEventPumpServer.start();
    }

    public void handleEvent(ServerEvent serverEvent) {
        try {
            genericEventStore.store(
                    serverEvent.getEventType(),
                    System.currentTimeMillis(),
                    mapper.writeValueAsString(serverEvent));   // TODO create marshaller
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public void commit() {
        eventEventsPumpServer.commit();
    }
}
