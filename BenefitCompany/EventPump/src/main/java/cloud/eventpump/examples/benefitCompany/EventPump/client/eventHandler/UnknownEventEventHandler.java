package cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler;

public interface UnknownEventEventHandler {

    void processUnknownEvent(GenericClientEvent genericClientEvent);
}
