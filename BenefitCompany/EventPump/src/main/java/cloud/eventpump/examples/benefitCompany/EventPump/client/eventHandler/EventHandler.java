package cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler;

public interface EventHandler<Event extends ClientEvent> {

    void processEvent(Event event);
}
