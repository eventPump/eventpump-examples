package cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EventHandlerHost {

    private final Map<String, EventHandlerWithClass<? extends ClientEvent>> eventTypeToEventHandler = new HashMap<>();
    private UnknownEventEventHandler unknownEventEventHandler;
    private final ObjectMapper mapper;

    public EventHandlerHost() {
        this.mapper = new ObjectMapper();
        this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public <Event extends ClientEvent> void register(String eventType, Class<Event> eventClass, EventHandler<Event> eventEventHandler) {
        eventTypeToEventHandler.put(eventType, new EventHandlerWithClass<>(eventEventHandler, eventClass));
    }

    public void registerUnknownEventHandler(UnknownEventEventHandler unknownEventEventHandler) {
        this.unknownEventEventHandler = unknownEventEventHandler;
    }

    public void process(GenericClientEvent genericClientEvent) {
        EventHandlerWithClass<? extends ClientEvent> eventHandlerWithClass = eventTypeToEventHandler.get(genericClientEvent.getEventType());
        if (eventHandlerWithClass != null) {
            processHandled(genericClientEvent, eventHandlerWithClass);
        } else {
            processUnhandled(genericClientEvent);
        }
    }

    private void processHandled(GenericClientEvent genericClientEvent, EventHandlerWithClass<? extends ClientEvent> eventHandlerWithClass) {
        ClientEvent clientEvent = null;
        try {
            clientEvent = mapper.readValue(genericClientEvent.getPayload(), eventHandlerWithClass.eventClass);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        clientEvent.setHeaderValues(genericClientEvent.getId(), genericClientEvent.getCreateUtcTs());
        eventHandlerWithClass.eventEventHandler.processEvent(clientEvent);
    }

    private void processUnhandled(GenericClientEvent genericClientEvent) {
        if (unknownEventEventHandler == null) {
            throw new RuntimeException("Unknown event eventType=" + genericClientEvent.getEventType() + " payload=" + genericClientEvent.getPayload());
        }
        unknownEventEventHandler.processUnknownEvent(genericClientEvent);
    }

    private static class EventHandlerWithClass<Event extends ClientEvent> {
        EventHandler eventEventHandler;
        Class<Event> eventClass;

        public EventHandlerWithClass(EventHandler<Event> eventEventHandler, Class<Event> eventClass) {
            this.eventEventHandler = eventEventHandler;
            this.eventClass = eventClass;
        }
    }
}
