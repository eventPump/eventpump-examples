package cloud.eventpump.examples.benefitCompany.EventPump.genericServerSpring;

import org.hibernate.EmptyInterceptor;
import org.hibernate.Transaction;
import org.hibernate.type.Type;

import java.io.Serializable;

public class AServerCommitInterceptor extends EmptyInterceptor {

    // TODO not sure this is appropriate solution

    private static CommitHandler commitHandler;
    private static ThreadLocal<Boolean> genericServerEventInTransaction = new ThreadLocal<>();

    @Override
    public synchronized boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        if (entity instanceof AServerEvent) {
            genericServerEventInTransaction.set(true);
        }
        return false;
    }

    @Override
    public synchronized void afterTransactionCompletion(Transaction tx) {
        if (genericServerEventInTransaction.get() != null) {
            genericServerEventInTransaction.set(null);
            commitHandler.commit();
        }
    }

    public static void setCommitHandler(CommitHandler commitHandler) {
        AServerCommitInterceptor.commitHandler = commitHandler;
    }
}
