package cloud.eventpump.examples.benefitCompany.EventPump.genericClientSpring;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class GenericAppliedId {

    @Id
    private long id;
    private long appliedId;

}
