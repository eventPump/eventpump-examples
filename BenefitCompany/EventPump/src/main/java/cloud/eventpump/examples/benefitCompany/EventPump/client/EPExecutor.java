package cloud.eventpump.examples.benefitCompany.EventPump.client;

public interface EPExecutor {

    long findLastAppliedDBId(long appliedIdId);

    void applyExecutorAndId(Runnable eventExecutor, long appliedIdId, long id);
}
