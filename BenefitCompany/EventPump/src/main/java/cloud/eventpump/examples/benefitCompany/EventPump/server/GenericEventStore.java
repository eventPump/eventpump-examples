package cloud.eventpump.examples.benefitCompany.EventPump.server;

import cloud.eventpump.server.EventsProvider;

public interface GenericEventStore extends EventsProvider<GenericServerEvent> {

    void store(String eventType, long currentTimeMillis, String payload);
}
