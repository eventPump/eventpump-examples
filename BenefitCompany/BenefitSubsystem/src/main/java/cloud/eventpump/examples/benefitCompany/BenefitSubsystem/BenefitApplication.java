package cloud.eventpump.examples.benefitCompany.BenefitSubsystem;

import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.BenefitService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {
        "cloud.eventpump.examples.benefitCompany.BenefitSubsystem",
        "cloud.eventpump.examples.benefitCompany.EventPump"})
@EnableJpaRepositories(basePackages = {
        "cloud.eventpump.examples.benefitCompany.BenefitSubsystem",
        "cloud.eventpump.examples.benefitCompany.EventPump"})
@EntityScan(basePackages = {
        "cloud.eventpump.examples.benefitCompany.BenefitSubsystem",
        "cloud.eventpump.examples.benefitCompany.EventPump"})
public class BenefitApplication implements CommandLineRunner {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private BenefitService benefitService;

    public static void main(String[] args) {
        SpringApplication.run(BenefitApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        waitForUser("not existing user");
    }

    private void waitForUser(String userName) throws InterruptedException {
        logger.info("Waiting for user : " + userName + " to be created ... ");
        while (!benefitService.employeeExist(userName)) {
            Thread.sleep(100);
        }
    }

}
