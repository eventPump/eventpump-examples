package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.employeeEPClient;

import cloud.eventpump.examples.benefitCompany.EventPump.client.EPClient;
import cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler.ClientEvent;
import cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler.EventHandler;
import cloud.eventpump.examples.benefitCompany.EventPump.genericClientSpring.GenericEPExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


@Component
public class HREmployeeEPClient {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${ep.hrEmployee.server.address}")
    private String epServerAddress;

    @Value("${ep.hrEmployee.server.port}")
    private int epServerPort;

    @Value("${ep.hrEmployee.client.name}")
    private String epClientName;

    @Autowired
    private GenericEPExecutor genericEPExecutor;
    private EPClient hrEmployeeEPClient = new EPClient();

    @PostConstruct
    public void start() {
        hrEmployeeEPClient.start(genericEPExecutor, 3, epServerAddress, epServerPort, epClientName);
    }

    public <Event extends ClientEvent> void registerEventHandler(String eventType, Class<Event> eventClass, EventHandler<Event> eventEventHandler) {
        hrEmployeeEPClient.register(eventType, eventClass, eventEventHandler);
    }

}
