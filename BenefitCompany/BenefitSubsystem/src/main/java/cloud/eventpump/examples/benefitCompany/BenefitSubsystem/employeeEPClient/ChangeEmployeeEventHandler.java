package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.employeeEPClient;

import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.BenefitService;
import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.employeeEPClient.events.EmployeeInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ChangeEmployeeEventHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final BenefitService benefitService;

    public ChangeEmployeeEventHandler(BenefitService benefitService, HREmployeeEPClient hrEmployeeEPClient) {
        this.benefitService = benefitService;
        hrEmployeeEPClient.registerEventHandler("EmployeeChanged_v0100", EmployeeInfo.class, this::processEvent);
    }

    public void processEvent(EmployeeInfo event) {
        logger.info("ChangeEmployee event arrived: {}", event.toString());
        benefitService.updateBenefitEmployee(event.getEmployeeId(), event.getName(), event.getEmail());
    }

}
