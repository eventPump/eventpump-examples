package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.employee;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@ToString
public class BenefitEmployee {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Embedded
    private EmployeeId employeeId ;
    private String name;
    private String email;

    public BenefitEmployee(EmployeeId employeeId, String name, String email) {
        this.employeeId = employeeId;
        this.name = name;
        this.email = email;
    }
}
