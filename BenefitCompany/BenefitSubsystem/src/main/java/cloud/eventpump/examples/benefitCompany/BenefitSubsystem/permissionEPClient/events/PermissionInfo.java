package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.permissionEPClient.events;


import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.employee.EmployeeId;
import cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler.ClientEvent;


public class PermissionInfo extends ClientEvent {

    private EmployeeId employeeId;
    private PermissionType permissionType;

    public EmployeeId getEmployeeId() {
        return employeeId;
    }

    public PermissionType getPermissionType() {
        return permissionType;
    }

    @Override
    public String toString() {
        return "PermissionInfo{" +
                "employeeId=" + employeeId +
                ", permissionType=" + permissionType +
                '}';
    }
}
