package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.benefitPermission;

import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.employee.EmployeeId;
import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.permissionEPClient.events.PermissionType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BenefitPermissionRepository extends CrudRepository<BenefitPermission, Long> {
    BenefitPermission findByEmployeeIdAndPermissionType(EmployeeId employeeId, PermissionType permissionType);

    List<BenefitPermission> findByEmployeeId(EmployeeId employeeId);
}
