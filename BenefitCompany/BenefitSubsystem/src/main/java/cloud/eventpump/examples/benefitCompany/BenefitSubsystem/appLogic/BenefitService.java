package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic;


import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.employee.BenefigEmployeeRepository;
import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.employee.BenefitEmployee;
import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.employee.BenefitEmployeeInfo;
import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.employee.EmployeeId;
import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.permissionEPClient.events.PermissionType;
import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.benefitPermission.BenefitPermission;
import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.benefitPermission.BenefitPermissionRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class BenefitService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final BenefitPermissionRepository benefitPermissionRepository;
    private final BenefigEmployeeRepository benefigEmployeeRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void addPermission(EmployeeId employeeId, PermissionType permissionType) {
        logger.info("addPermission : {}", employeeId);
        BenefitPermission benefitPermission = benefitPermissionRepository.findByEmployeeIdAndPermissionType(employeeId, permissionType);
        if (benefitPermission == null) {
            benefitPermissionRepository.save(new BenefitPermission(employeeId, permissionType));
            show("addPermission");
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void revokePermission(EmployeeId employeeId, PermissionType permissionType) {
        logger.info("revokePermission : {}", employeeId);
        BenefitPermission benefitPermission = benefitPermissionRepository.findByEmployeeIdAndPermissionType(employeeId, permissionType);
        if (benefitPermission != null) {
            benefitPermissionRepository.delete(benefitPermission);
        }
        show("revokePermission");
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void createBenefitEmployee(EmployeeId employeeId, String name, String email) {
        benefigEmployeeRepository.save(new BenefitEmployee(employeeId, name, email));
        show("createBenefitEmployee");
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateBenefitEmployee(EmployeeId employeeId, String name, String email) {
        BenefitEmployee benefitEmployee = benefigEmployeeRepository.findByEmployeeId(employeeId);
        benefitEmployee.setName(name);
        benefitEmployee.setEmail(email);
        show("updateBenefitEmployee");
    }

    public List<BenefitEmployeeInfo> getAllEmployees() {
        List<BenefitEmployeeInfo> result = new ArrayList<>();
        for (BenefitEmployee benefitEmployee : benefigEmployeeRepository.findAll()) {
            result.add(new BenefitEmployeeInfo(benefitEmployee.getEmployeeId(), benefitEmployee.getName(), benefitEmployee.getEmail()));
        }
        return result;
    }

    public List<PermissionType> getPermissions(String name) {
        BenefitEmployee permissionFor = getEmployee(name);
        return benefitPermissionRepository.findByEmployeeId(permissionFor.getEmployeeId()).stream()
                .map(BenefitPermission::getPermissionType)
                .collect(Collectors.toList());
    }

    public boolean employeeExist(String name) {
        return benefigEmployeeRepository.findByName(name).isPresent();
    }

    private BenefitEmployee getEmployee(String name) {
        return benefigEmployeeRepository.findByName(name).orElseThrow(() -> new RuntimeException("User not found"));
    }

    private void show(String action) {
        StringBuilder message = new StringBuilder();
        List<BenefitEmployeeInfo> allEmployees = getAllEmployees();
        for (BenefitEmployeeInfo employee : allEmployees) {
            message.append("\n\t" + employee.getName() + " " + employee.getEmail() + " " + employee.getEmployeeId() );

            StringJoiner stringJoiner = new StringJoiner(", ");
            getPermissions(employee.getName()).forEach(a -> stringJoiner.add(a.toString()));
            if (stringJoiner.length() > 0) {
                message.append("\n\t\t" + stringJoiner);
            }
        }
        logger.info("BenefitService action: {} {}", action, message);
    }
}
