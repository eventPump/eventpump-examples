package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.employee;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BenefigEmployeeRepository extends CrudRepository<BenefitEmployee,Long> {

    Optional<BenefitEmployee> findByName(String name ) ;

    BenefitEmployee findByEmployeeId(EmployeeId employeeId);
}
