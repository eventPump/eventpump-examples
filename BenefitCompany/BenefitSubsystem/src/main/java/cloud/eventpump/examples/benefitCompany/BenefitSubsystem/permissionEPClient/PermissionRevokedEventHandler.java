package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.permissionEPClient;

import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.permissionEPClient.events.PermissionRevoked;
import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.BenefitService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PermissionRevokedEventHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final BenefitService employeeService;

    public PermissionRevokedEventHandler(BenefitService employeeService, PermissionEPClient permissionEPClient) {
        this.employeeService = employeeService;
        permissionEPClient.registerEventHandler("PermissionRevoked_v0100", PermissionRevoked.class, this::processEvent);
    }

    private void processEvent(PermissionRevoked event) {
        logger.info("PermissionRevoked event arrived: {}", event.toString());
        employeeService.revokePermission(event.getEmployeeId(), event.getPermissionType());
    }
}
