package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.employeeEPClient.events;


import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.employee.EmployeeId;
import cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler.ClientEvent;

public class EmployeeInfo extends ClientEvent {
    private EmployeeId employeeId;
    private String name;
    private String email;

    public EmployeeId getEmployeeId() {
        return employeeId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "EmployeeInfo{" +
                "employeeId=" + employeeId +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
