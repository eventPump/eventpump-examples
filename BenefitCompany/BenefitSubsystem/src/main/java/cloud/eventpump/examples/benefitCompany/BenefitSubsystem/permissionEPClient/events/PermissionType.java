package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.permissionEPClient.events;

public enum PermissionType {
    // TODO should be removed from client,or errors properly handled
    ManageEmployees, ManageRights, ManageBenefits, ManageSalaries
}

