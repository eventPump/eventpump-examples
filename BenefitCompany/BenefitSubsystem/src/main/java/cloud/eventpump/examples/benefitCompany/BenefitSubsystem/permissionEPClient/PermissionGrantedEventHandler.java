package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.permissionEPClient;


import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.BenefitService;
import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.permissionEPClient.events.PermissionGranted;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PermissionGrantedEventHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final BenefitService benefitService;

    public PermissionGrantedEventHandler(BenefitService benefitService, PermissionEPClient permissionEPClient) {
        this.benefitService = benefitService;
        permissionEPClient.registerEventHandler("PermissionGranted_v0100", PermissionGranted.class, this::processEvent);
    }

    public void processEvent(PermissionGranted event) {
        logger.info("PermissionGranted event arrived: {}", event.toString());
        benefitService.addPermission( event.getEmployeeId(), event.getPermissionType());
    }
}
