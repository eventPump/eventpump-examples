package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.benefitPermission;

import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.employee.EmployeeId;
import cloud.eventpump.examples.benefitCompany.BenefitSubsystem.permissionEPClient.events.PermissionType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@ToString
public class BenefitPermission {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Embedded
    private EmployeeId employeeId ;
    private PermissionType permissionType;

    public BenefitPermission(EmployeeId employeeId, PermissionType permissionType) {
        this.employeeId = employeeId;
        this.permissionType = permissionType;
    }
}
