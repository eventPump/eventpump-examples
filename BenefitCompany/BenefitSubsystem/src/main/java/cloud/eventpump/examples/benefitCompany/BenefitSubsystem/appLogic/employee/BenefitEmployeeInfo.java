package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.appLogic.employee;

import lombok.AllArgsConstructor;
import lombok.ToString;
import lombok.Value;


@Value
@AllArgsConstructor
@ToString
public class BenefitEmployeeInfo {

    private EmployeeId employeeId;
    private String name;
    private String email;

}
