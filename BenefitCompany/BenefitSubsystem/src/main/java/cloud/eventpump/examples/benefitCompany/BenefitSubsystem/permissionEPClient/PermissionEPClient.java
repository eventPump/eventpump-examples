package cloud.eventpump.examples.benefitCompany.BenefitSubsystem.permissionEPClient;

import cloud.eventpump.examples.benefitCompany.EventPump.client.EPClient;
import cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler.ClientEvent;
import cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler.EventHandler;
import cloud.eventpump.examples.benefitCompany.EventPump.genericClientSpring.GenericEPExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PermissionEPClient {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${ep.permission.server.address}")
    private String epPermissionServerAddress;

    @Value("${ep.permission.server.port}")
    private int epPermissionServerPort;

    @Value("${ep.permission.client.name}")
    private String epPermissionClientName;

    @Autowired
    private GenericEPExecutor genericEPExecutor;
    private EPClient permissionEPClient = new EPClient();

    @PostConstruct
    public void start() {
        permissionEPClient.start(genericEPExecutor, 4, epPermissionServerAddress, epPermissionServerPort, epPermissionClientName);
    }

    public <Event extends ClientEvent> void registerEventHandler(String eventType, Class<Event> eventClass, EventHandler<Event> eventEventHandler) {
        permissionEPClient.register(eventType, eventClass, eventEventHandler);
    }

}
