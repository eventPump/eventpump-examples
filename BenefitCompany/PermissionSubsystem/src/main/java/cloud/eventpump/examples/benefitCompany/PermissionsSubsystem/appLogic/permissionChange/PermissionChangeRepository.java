package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permissionChange;

import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee.PermissionsEmployee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionChangeRepository extends CrudRepository<PermissionChange,Long> {

    List<PermissionChange> findByPermissionForAndTimestampUtcGreaterThanEqualAndTimestampUtcLessThanEqual(PermissionsEmployee permissionFor, long t1, long t2) ;
}
