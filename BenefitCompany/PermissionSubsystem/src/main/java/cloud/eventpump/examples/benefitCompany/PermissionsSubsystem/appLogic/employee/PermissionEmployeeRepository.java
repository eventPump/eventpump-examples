package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PermissionEmployeeRepository extends CrudRepository<PermissionsEmployee,Long> {

    Optional<PermissionsEmployee> findByName(String name ) ;

    PermissionsEmployee findByEmployeeId(EmployeeId employeeId);
}
