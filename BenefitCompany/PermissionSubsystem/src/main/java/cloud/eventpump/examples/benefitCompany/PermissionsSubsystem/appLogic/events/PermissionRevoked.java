package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.events;

import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee.EmployeeId;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permission.PermissionType;
import lombok.Value;

@Value
public class PermissionRevoked extends PermissionInfo {

    public PermissionRevoked(EmployeeId employeeId, PermissionType permissionType) {
        super("PermissionRevoked_v0100", employeeId, permissionType);
    }
}
