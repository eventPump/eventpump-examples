package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.permissionEPServer;

import cloud.eventpump.examples.benefitCompany.EventPump.genericServerSpring.AEventStore;
import cloud.eventpump.examples.benefitCompany.EventPump.genericServerSpring.AServerCommitInterceptor;
import cloud.eventpump.examples.benefitCompany.EventPump.genericServerSpring.CommitHandler;
import cloud.eventpump.examples.benefitCompany.EventPump.server.EPServer;
import cloud.eventpump.examples.benefitCompany.EventPump.server.EventHandler;
import cloud.eventpump.examples.benefitCompany.EventPump.server.ServerEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
public class PermissionEPServer implements EventHandler, CommitHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${ep.permission.server.address}")
    private String epServerAddress;

    @Value("${ep.permission.server.port}")
    private int epServerPort;

    @Value("${ep.permission.StatisticsServer.port}")
    private int epStatisticsServerPort;

    @Autowired
    private AEventStore eventStore;

    private EPServer epServer;

    @PostConstruct
    public void start() throws IOException {
        AServerCommitInterceptor.setCommitHandler(this);
        epServer = new EPServer(epServerAddress, epServerPort, epStatisticsServerPort, eventStore);
        epServer.start();
    }

    @Override
    public void handleEvent(ServerEvent serverEvent) {
        logger.info("Event raised : {}", serverEvent);
        epServer.handleEvent(serverEvent);
    }

    public void commit() {
        logger.info("commit has happened");
        epServer.commit();
    }
}
