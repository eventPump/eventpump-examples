package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permissionChange;

import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permission.PermissionType;
import lombok.*;

@Value
@AllArgsConstructor
@ToString
public class PermissionChangeInfo {

    private PermissionType permissionType;
    private PermissionChangeType permissionChangeType;
    private String changedByName ;
    private String changedByEmail ;
    private long timestampUtc ;

}
