package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic;

import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee.EmployeeId;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee.PermissionEmployeeInfo;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee.PermissionEmployeeRepository;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee.PermissionsEmployee;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.events.PermissionGranted;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.events.PermissionRevoked;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permission.Permission;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permission.PermissionRepository;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permission.PermissionType;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permissionChange.PermissionChange;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permissionChange.PermissionChangeInfo;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permissionChange.PermissionChangeRepository;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permissionChange.PermissionChangeType;
import cloud.eventpump.examples.benefitCompany.EventPump.server.EventHandler;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PermissionService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final PermissionRepository permissionRepository;
    private final PermissionChangeRepository permissionChangeRepository;
    private final PermissionEmployeeRepository permissionEmployeeRepository;
    private final EventHandler eventHandler;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void addPermission(String setForUserName, PermissionType permissionType, String setByUserName) {
        PermissionsEmployee setBy = getEmployee(setByUserName);
        checkPermission( setBy, PermissionType.ManageRights ) ;
        PermissionsEmployee setFor = getEmployee(setForUserName);
        permissionRepository.save(new Permission(permissionType, setFor));
        permissionChangeRepository.save(new PermissionChange(permissionType, PermissionChangeType.Granted, System.currentTimeMillis(), setFor, setBy));
        eventHandler.handleEvent(new PermissionGranted(setFor.getEmployeeId(), permissionType));
        show("addPermission");
    }



    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void revokePermission(String revokeFromUserName, PermissionType permissionType, String revokeByUserName) {
        PermissionsEmployee revokeBy = getEmployee(revokeByUserName);
        checkPermission( revokeBy, PermissionType.ManageRights ) ;
        PermissionsEmployee revokeFrom = getEmployee(revokeFromUserName);
        Permission permission = permissionRepository.findByPermissionForAndPermissionType(revokeFrom, permissionType);
        if (permission != null) {
            permissionRepository.delete(permission);
            permissionChangeRepository.save(new PermissionChange(permissionType, PermissionChangeType.Revoked, System.currentTimeMillis(), revokeFrom, revokeBy));
            eventHandler.handleEvent(new PermissionRevoked(revokeFrom.getEmployeeId(), permissionType));
            show("revokePermission");
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void createPermissionEmployee(EmployeeId employeeId, String name, String email, boolean isRoot) {
        permissionEmployeeRepository.save(new PermissionsEmployee(employeeId, name, email, isRoot));
        show("createPermissionEmployee");
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updatePermissionEmployee(EmployeeId employeeId, String name, String email) {
        PermissionsEmployee permissionsEmployee = permissionEmployeeRepository.findByEmployeeId(employeeId);
        permissionsEmployee.setName(name);
        permissionsEmployee.setEmail(email);
        show("updatePermissionEmployee");
    }

    public List<PermissionEmployeeInfo> getAllEmployees() {
        List<PermissionEmployeeInfo> result = new ArrayList<>();
        for (PermissionsEmployee permissionsEmployee : permissionEmployeeRepository.findAll()) {
            result.add(new PermissionEmployeeInfo(permissionsEmployee.getEmployeeId(), permissionsEmployee.getName(), permissionsEmployee.getEmail(), permissionsEmployee.isRoot()));
        }
        return result;
    }


    public List<PermissionType> getPermissions(String name) {
        PermissionsEmployee permissionFor = getEmployee(name);
        return permissionRepository.findByPermissionFor(permissionFor).stream()
                .map(Permission::getPermissionType)
                .collect(Collectors.toList());
    }

    public List<PermissionChangeInfo> getPermissionChanges(String name, Date from, Date to) {
        PermissionsEmployee permissionFor = getEmployee(name);
        return permissionChangeRepository.findByPermissionForAndTimestampUtcGreaterThanEqualAndTimestampUtcLessThanEqual(permissionFor, from.getTime(), to.getTime()).stream()
                .map(this::convertToPermissionChangeInfo)
                .collect(Collectors.toList());
    }

    private void checkPermission(PermissionsEmployee employee, PermissionType permissionType) {
        if ( employee.isRoot() || null != permissionRepository.findByPermissionForAndPermissionType(employee, permissionType) ) {
            logger.info("Employee : {} authorized for : {}", employee.getName(), permissionType.toString());
        } else {
            throw new RuntimeException("Employee : " + employee.getName() + " not authorized : " + permissionType.toString());
        }
    }

    public boolean employeeExist(String name ) {
         return permissionEmployeeRepository.findByName(name).isPresent() ;
    }

    private PermissionChangeInfo convertToPermissionChangeInfo(PermissionChange permissionChange) {
        return new PermissionChangeInfo(
                permissionChange.getPermissionType(),
                permissionChange.getPermissionChangeType(),
                permissionChange.getChangedBy().getName(),
                permissionChange.getChangedBy().getEmail(),
                permissionChange.getTimestampUtc());
    }

    private PermissionsEmployee getEmployee(String name) {
        return permissionEmployeeRepository.findByName(name).orElseThrow(() -> new RuntimeException("Employee not found"));
    }

    private void show(String action) {
        StringBuilder message = new StringBuilder() ;
        List<PermissionEmployeeInfo> allEmployees = getAllEmployees();
        for (PermissionEmployeeInfo employee : allEmployees) {
            message.append("\n\t" + employee.getName() + " " + employee.getEmail() + " " + employee.getEmployeeId() + " " + employee.isRoot());

            StringJoiner stringJoiner = new StringJoiner(", ");
            getPermissions(employee.getName()).forEach(a -> stringJoiner.add(a.toString()));
            if (stringJoiner.length() > 0) {
                message.append("\n\t\t" + stringJoiner);
            }

            StringJoiner stringJoiner1 = new StringJoiner(", ");
            getPermissionChanges(employee.getName(), new Date(Long.MIN_VALUE), new Date(Long.MAX_VALUE)).forEach(ac -> stringJoiner1.add(
                    "{" + ac.getChangedByName() + ", " + ac.getPermissionType() + ", " + ac.getPermissionChangeType() + "}"));
            if (stringJoiner1.length() > 0) {
                message.append("\n\t\t" + stringJoiner1);
            }
        }
        logger.info("PermissionService action: {} {}", action, message );
    }
}
