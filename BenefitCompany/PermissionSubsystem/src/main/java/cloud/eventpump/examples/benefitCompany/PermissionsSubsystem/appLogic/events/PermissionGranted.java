package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.events;

import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permission.PermissionType;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee.EmployeeId;
import lombok.Value;

@Value
public class PermissionGranted extends PermissionInfo {

    public PermissionGranted(EmployeeId employeeId, PermissionType permissionType) {
        super("PermissionGranted_v0100", employeeId, permissionType);
    }
}
