package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.events;

import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permission.PermissionType;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee.EmployeeId;
import cloud.eventpump.examples.benefitCompany.EventPump.server.ServerEvent;

public class PermissionInfo extends ServerEvent {

    private EmployeeId employeeId;
    private PermissionType permissionType;

    public PermissionInfo(String eventType, EmployeeId employeeId, PermissionType permissionType) {
        super(eventType);
        this.employeeId = employeeId;
        this.permissionType = permissionType;
    }

    public EmployeeId getEmployeeId() {
        return employeeId;
    }

    public PermissionType getPermissionType() {
        return permissionType;
    }
}
