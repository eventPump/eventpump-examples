package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permissionChange;

import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee.PermissionsEmployee;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permission.PermissionType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@ToString
public class PermissionChange {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private PermissionType permissionType;
    private PermissionChangeType permissionChangeType;
    private long timestampUtc ;

    @ManyToOne(fetch=FetchType.EAGER)
    private PermissionsEmployee permissionFor;

    @ManyToOne(fetch=FetchType.EAGER)
    private PermissionsEmployee changedBy ;

    public PermissionChange(PermissionType permissionType, PermissionChangeType permissionChangeType, long timestampUtc, PermissionsEmployee permissionFor, PermissionsEmployee changedBy) {
        this.permissionType = permissionType;
        this.permissionChangeType = permissionChangeType;
        this.timestampUtc = timestampUtc ;
        this.permissionFor = permissionFor;
        this.changedBy = changedBy;
    }
}
