package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem;

import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.PermissionService;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permission.PermissionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {
        "cloud.eventpump.examples.benefitCompany.PermissionsSubsystem",
        "cloud.eventpump.examples.benefitCompany.EventPump"})
@EnableJpaRepositories(basePackages = {
        "cloud.eventpump.examples.benefitCompany.PermissionsSubsystem",
        "cloud.eventpump.examples.benefitCompany.EventPump"})
@EntityScan(basePackages = {
        "cloud.eventpump.examples.benefitCompany.PermissionsSubsystem",
        "cloud.eventpump.examples.benefitCompany.EventPump"})
public class PermissionApplication implements CommandLineRunner {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private PermissionService permissionService;


    public static void main(String[] args) {
        SpringApplication.run(PermissionApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        waitForUser("IT-Manager");

        permissionService.addPermission("HR-Manager", PermissionType.ManageEmployees, "root");
        permissionService.addPermission("HR-Manager", PermissionType.ManageBenefits, "root");
        permissionService.addPermission("HR-Manager", PermissionType.ManageSalaries, "root");
        permissionService.addPermission("HR-Manager", PermissionType.ManageRights, "root");

        waitForUser("hr1");
        permissionService.addPermission("hr1", PermissionType.ManageBenefits, "HR-Manager");
        permissionService.addPermission("hr1", PermissionType.ManageSalaries, "HR-Manager");

        waitForUser("hr2");
        permissionService.addPermission("hr2", PermissionType.ManageBenefits, "HR-Manager");

        waitForUser("not existing user");
    }

    private void waitForUser(String userName) throws InterruptedException {
        logger.info("Waiting for user : " + userName + " to be created ... ");
        while (!permissionService.employeeExist(userName)) {
            Thread.sleep(100);
        }
    }

}
