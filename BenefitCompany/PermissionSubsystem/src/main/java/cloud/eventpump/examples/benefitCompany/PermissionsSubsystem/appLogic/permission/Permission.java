package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permission;

import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee.PermissionsEmployee;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames = {"permission_type" , "permission_for_id"})})
@Data
@NoArgsConstructor
@ToString
public class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "permission_type")
    private PermissionType permissionType;

    @ManyToOne(fetch=FetchType.EAGER)
    private PermissionsEmployee permissionFor ;

    public Permission(PermissionType permissionType, PermissionsEmployee permissionFor) {
        this.permissionType = permissionType;
        this.permissionFor = permissionFor;
    }
}
