package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permission;

import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee.PermissionsEmployee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionRepository extends CrudRepository<Permission, Long> {

    Permission findByPermissionForAndPermissionType(PermissionsEmployee permissionFor, PermissionType permissionType) ;
    List<Permission> findByPermissionFor(PermissionsEmployee permissionsEmployee) ;
}
