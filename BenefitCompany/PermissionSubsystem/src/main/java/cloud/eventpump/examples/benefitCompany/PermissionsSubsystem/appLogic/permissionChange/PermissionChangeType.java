package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permissionChange;

public enum PermissionChangeType {
    Granted, Revoked
}
