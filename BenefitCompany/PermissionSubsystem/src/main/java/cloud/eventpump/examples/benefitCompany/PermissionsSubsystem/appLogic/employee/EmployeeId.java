package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee;

import lombok.*;

import javax.persistence.Embeddable;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Embeddable
@EqualsAndHashCode
public class EmployeeId {

    private static String ID_PREFIX = "employee-";

    public static EmployeeId create() {
        return new EmployeeId(ID_PREFIX + UUID.randomUUID());
    }

    private String employeeId;

    public static String getIdPrefix() {
        return ID_PREFIX;
    }

}
