package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.employeeEPClient;

import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.PermissionService;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.employeeEPClient.events.EmployeeInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ChangeEmployeeEventHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final PermissionService permissionService;

    public ChangeEmployeeEventHandler(PermissionService permissionService, HREmployeeEPClient hrEmployeeEPClient) {
        this.permissionService = permissionService;
        hrEmployeeEPClient.register("EmployeeChanged_v0100", EmployeeInfo.class, this::processEvent);
    }

    public void processEvent(EmployeeInfo event) {
        logger.info("ChangeEmployee event arrived: {}", event.toString());
        permissionService.updatePermissionEmployee(event.getEmployeeId(), event.getName(), event.getEmail());
    }

}
