package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.employeeEPClient.events;


import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee.EmployeeId;
import cloud.eventpump.examples.benefitCompany.EventPump.client.eventHandler.ClientEvent;

public class EmployeeInfo extends ClientEvent {
    private EmployeeId employeeId;
    private String name;
    private String email;
    private boolean root;

    public EmployeeId getEmployeeId() {
        return employeeId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public boolean isRoot() {
        return root;
    }

    @Override
    public String toString() {
        return "EmployeeInfo{" +
                "employeeId=" + employeeId +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", isRoot=" + root +
                '}';
    }
}
