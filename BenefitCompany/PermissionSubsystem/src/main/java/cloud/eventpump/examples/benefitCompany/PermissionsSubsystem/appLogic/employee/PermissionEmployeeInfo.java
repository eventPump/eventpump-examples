package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee;

import lombok.AllArgsConstructor;
import lombok.ToString;
import lombok.Value;


@Value
@AllArgsConstructor
@ToString
public class PermissionEmployeeInfo {

    private EmployeeId employeeId;
    private String name;
    private String email;
    private boolean isRoot ;

}
