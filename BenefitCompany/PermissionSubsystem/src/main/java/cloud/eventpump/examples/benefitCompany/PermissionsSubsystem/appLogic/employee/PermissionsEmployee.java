package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.employee;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@ToString
public class PermissionsEmployee {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Embedded
    private EmployeeId employeeId ;
    private String name;
    private String email;
    private boolean isRoot ;

    public PermissionsEmployee(EmployeeId employeeId, String name, String email, boolean isRoot) {
        this.employeeId = employeeId;
        this.name = name;
        this.email = email;
        this.isRoot = isRoot ;
    }
}
