package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.permission;

public enum PermissionType {
    ManageEmployees, ManageRights, ManageBenefits, ManageSalaries
}

