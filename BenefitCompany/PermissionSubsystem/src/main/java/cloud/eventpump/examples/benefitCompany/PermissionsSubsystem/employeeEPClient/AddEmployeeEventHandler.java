package cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.employeeEPClient;

import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.appLogic.PermissionService;
import cloud.eventpump.examples.benefitCompany.PermissionsSubsystem.employeeEPClient.events.EmployeeInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AddEmployeeEventHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final PermissionService permissionService;

    public AddEmployeeEventHandler(PermissionService permissionService, HREmployeeEPClient hrEmployeeEPClient) {
        this.permissionService = permissionService;
        hrEmployeeEPClient.register("EmployeeAdded_v0100", EmployeeInfo.class, this::processEvent);
    }

    public void processEvent(EmployeeInfo event) {
        logger.info("AddEmployee event arrived: {}", event.toString());
        permissionService.createPermissionEmployee(event.getEmployeeId(), event.getName(), event.getEmail(), event.isRoot());
    }

}
