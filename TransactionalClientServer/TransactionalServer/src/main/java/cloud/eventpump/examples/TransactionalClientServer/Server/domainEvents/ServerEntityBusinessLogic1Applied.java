package cloud.eventpump.examples.TransactionalClientServer.Server.domainEvents;

import lombok.Getter;

@Getter
public class ServerEntityBusinessLogic1Applied extends ServerEvent {

    private final int field1;

    public ServerEntityBusinessLogic1Applied(String id, int field1) {
        super(id, "BusinessLogic1Applied");
        this.field1 = field1;
    }

}
