package cloud.eventpump.examples.TransactionalClientServer.Server.businessLogic;

import cloud.eventpump.examples.TransactionalClientServer.Server.domainEvents.*;
import cloud.eventpump.examples.TransactionalClientServer.Server.eventStore.EventStore;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class ServerAggregate {

    private final EventStore eventStore;

    private String id;
    private String unchangeableField;
    private int field1;
    private String field2;

    private boolean deleted;

    public ServerAggregate(EventStore eventStore) {
        this.eventStore = eventStore;
    }

    public String create(String unchangeableField) {
        this.id = UUID.randomUUID().toString();
        this.unchangeableField = unchangeableField;
        this.field1 = 1000;
        this.field2 = "";

        raiseEvent(new ServerEntityCreated(id, unchangeableField, field1, field2));
        return this.id;
    }

    public void businessLogic1(int value) {
        if (!field2.trim().isEmpty()) {
            field1 = value;
            raiseEvent(new ServerEntityBusinessLogic1Applied(id, field1));
        } else {
            throw new RuntimeException("BusinessLogic1");
        }
    }


    public void businessLogic2(String value) {
        if (field1 > 0) {
            field2 = value;
            raiseEvent(new ServerEntityBusinessLogic2Applied(id, field2));
        } else {
            throw new RuntimeException("BusinessLogic2");
        }
    }

    public void delete() {
        if (!deleted) {
            deleted = true;
            raiseEvent(new ServerEntityDeleted(id));
        } else {
            throw new RuntimeException("Delete");
        }
    }

    private void raiseEvent(ServerEvent serverEvent) {
        eventStore.store(serverEvent);
    }

}
