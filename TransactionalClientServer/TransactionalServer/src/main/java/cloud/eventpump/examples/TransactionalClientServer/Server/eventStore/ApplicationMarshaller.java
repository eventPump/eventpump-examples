package cloud.eventpump.examples.TransactionalClientServer.Server.eventStore;

import cloud.eventpump.examples.TransactionalClientServer.Server.domainEvents.ServerEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.stereotype.Component;

@Component
public class ApplicationMarshaller {

    private final ObjectMapper mapper;

    public ApplicationMarshaller() {
        this.mapper = new ObjectMapper();
        this.mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }

    public String marshall(ServerEvent serverEvent) {
        try {
            return mapper.writeValueAsString(serverEvent);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
