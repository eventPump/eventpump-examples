package cloud.eventpump.examples.TransactionalClientServer.Server.domainEvents;

public class ServerEntityDeleted extends ServerEvent {

    public ServerEntityDeleted(String id) {
        super(id, "EntityDeleted");
    }
}
