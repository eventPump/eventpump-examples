package cloud.eventpump.examples.TransactionalClientServer.Server.eventPump;

import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.ServerConfig;
import cloud.eventpump.statisticsServer.StatisticsEventPumpServer;
import cloud.eventpump.statisticsServer.StatisticsWebConfig;
import cloud.eventpump.statisticsServer.StatisticsWebEventPumpServer;
import cloud.eventpump.tcpServer.TcpEventsPumpServer;
import cloud.eventpump.tcpServer.TcpServerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.InetSocketAddress;

@Component
public class EventPumpServer {

    @Value("${ep.server.address}")
    private String epServerAddress;
    @Value("${ep.server.port}")
    private int epServerPort;
    @Value("${ep.statistics.server.port}")
    private int epStatisticsServerPort;

    private EventsPumpServer<GenericServerEvent> eventEventsPumpServer;
    private TcpEventsPumpServer<GenericServerEvent> tcpEventsPumpServer;
    private StatisticsEventPumpServer<GenericServerEvent> statisticsEventPumpServer;
    private StatisticsWebEventPumpServer<GenericServerEvent> statisticsHtmlEventPumpServer;

    @Autowired
    private GenericServerEventProvider genericServerEventProvider;

    @PostConstruct
    public void postConstruct() throws IOException {
        CommitInterceptor.setCommitHandler(this);
        eventEventsPumpServer = new EventsPumpServer<>(genericServerEventProvider, new GenericServerEventAdapter(), new ServerConfig());
        TcpServerConfig<GenericServerEvent> tcpServerConfig = new TcpServerConfig<>(new GenericServerEventMarshaller());
        tcpServerConfig.setServerAddress(new InetSocketAddress(epServerAddress, epServerPort));
        tcpEventsPumpServer = new TcpEventsPumpServer<>(eventEventsPumpServer, tcpServerConfig);
        statisticsEventPumpServer = new StatisticsEventPumpServer<>(eventEventsPumpServer);
        StatisticsWebConfig statisticsWebConfig = new StatisticsWebConfig();
        statisticsWebConfig.setPort(epStatisticsServerPort);
        statisticsHtmlEventPumpServer = new StatisticsWebEventPumpServer<>(statisticsEventPumpServer, statisticsWebConfig);
        tcpEventsPumpServer.start();
        statisticsHtmlEventPumpServer.start();
    }

    public void commit() {
        eventEventsPumpServer.commit();
        System.out.println("Commit happened");
    }
}
