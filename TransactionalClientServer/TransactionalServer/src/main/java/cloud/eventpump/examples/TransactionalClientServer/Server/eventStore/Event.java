package cloud.eventpump.examples.TransactionalClientServer.Server.eventStore;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@ToString
@Entity
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String eventId;
    private String eventType;
    private long createTimestampUtc;
    private String payload;

    public Event(String eventId, String eventType, long createTimestampUtc, String payload) {
        this.eventId = eventId;
        this.eventType = eventType;
        this.createTimestampUtc = createTimestampUtc;
        this.payload = payload;
    }
}
