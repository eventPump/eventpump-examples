package cloud.eventpump.examples.TransactionalClientServer.Server.eventStore;

import cloud.eventpump.examples.TransactionalClientServer.Server.domainEvents.ServerEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class EventStore {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private ApplicationMarshaller applicationMarshaller;

    @Transactional(propagation = Propagation.REQUIRED)
    public void store(ServerEvent serverEvent) {
        System.out.println("EventStored: " + serverEvent.getClass().getName());
        String payload = applicationMarshaller.marshall(serverEvent);
        eventRepository.save(new Event(
                serverEvent.getId(),
                serverEvent.getEventType(),
                serverEvent.getTimestampUtc(),
                payload));
    }

}
