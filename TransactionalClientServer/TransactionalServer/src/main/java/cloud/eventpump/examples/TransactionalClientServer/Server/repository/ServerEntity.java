package cloud.eventpump.examples.TransactionalClientServer.Server.repository;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class ServerEntity {

    @Id
    private String id;

    private String unchangeableField;
    private int field1;
    private String field2;

}
