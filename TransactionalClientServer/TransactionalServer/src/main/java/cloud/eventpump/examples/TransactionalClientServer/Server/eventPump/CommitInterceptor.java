package cloud.eventpump.examples.TransactionalClientServer.Server.eventPump;

import cloud.eventpump.examples.TransactionalClientServer.Server.eventStore.Event;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Transaction;
import org.hibernate.type.Type;

import java.io.Serializable;

public class CommitInterceptor extends EmptyInterceptor {

    private static EventPumpServer eventPumpServer;
    private static ThreadLocal<Boolean> genericServerEventInTransaction = new ThreadLocal<>();

    @Override
    public synchronized boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        if (entity instanceof Event) {
            genericServerEventInTransaction.set(true);
        }
        return false;
    }

    @Override
    public synchronized void afterTransactionCompletion(Transaction tx) {
        if (genericServerEventInTransaction.get() != null) {
            genericServerEventInTransaction.set(null);
            eventPumpServer.commit();
        }
    }

    public static void setCommitHandler(EventPumpServer eventPumpServer) {
        CommitInterceptor.eventPumpServer = eventPumpServer;
    }
}
