package cloud.eventpump.examples.TransactionalClientServer.Server.domainEvents;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

@Getter
public class ServerEvent {

    @JsonIgnore
    private final long timestampUtc;

    @JsonIgnore
    private final String eventType ;

    private final String id;

    public ServerEvent(String id, String eventType) {
        this.eventType = eventType ;
        this.timestampUtc = System.currentTimeMillis();
        this.id = id;
    }

}
