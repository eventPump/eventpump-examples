package cloud.eventpump.examples.TransactionalClientServer.Server.domainEvents;

import lombok.Getter;

@Getter
public class ServerEntityBusinessLogic2Applied extends ServerEvent {

    private final String field2;

    public ServerEntityBusinessLogic2Applied(String id, String field2) {
        super(id, "BusinessLogic2Applied");
        this.field2 = field2;
    }
}
