package cloud.eventpump.examples.TransactionalClientServer.Server.eventPump;

import cloud.eventpump.common.buffers.PrimitivesWriteBuffer;
import cloud.eventpump.common.marshaller.EventMarshaller;


class GenericServerEventMarshaller implements EventMarshaller<GenericServerEvent> {

    @Override
    public void marshall(GenericServerEvent genericServerEvent, PrimitivesWriteBuffer primitivesWriteBuffer) throws Exception {
        primitivesWriteBuffer.putLong(genericServerEvent.getEventId());
        primitivesWriteBuffer.putLong(genericServerEvent.getCreateUtcTs());
        primitivesWriteBuffer.putString(genericServerEvent.getServerEventType());
        primitivesWriteBuffer.putString(genericServerEvent.getPayload());
    }
}
