package cloud.eventpump.examples.TransactionalClientServer.Server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionalServerApplication implements ApplicationRunner {

    @Autowired
    private ServerApplicationService serverApplicationService;

    public static void main(String[] args) {
        SpringApplication.run(TransactionalServerApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        String a1Id = serverApplicationService.create("unchangeableFiled-1");

        String a2Id = serverApplicationService.create("unchangeableFiled-2");

        serverApplicationService.businessLogic2(a1Id, "BusinessLogic2 applied for a1");
        serverApplicationService.businessLogic1(a1Id, 0);

        serverApplicationService.businessLogic2(a2Id, "BusinessLogic2 applied for a2");

        serverApplicationService.delete(a2Id);
    }

}
