package cloud.eventpump.examples.TransactionalClientServer.Server.eventPump;

import cloud.eventpump.examples.TransactionalClientServer.Server.eventStore.Event;
import cloud.eventpump.examples.TransactionalClientServer.Server.eventStore.EventRepository;
import cloud.eventpump.server.EventsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GenericServerEventProvider implements EventsProvider<GenericServerEvent> {

    @Autowired
    private EventRepository eventRepository;

    @Override
    public List<GenericServerEvent> provideEvents(long dbId, int maxCount) {
        List<Event> events = eventRepository.findEvents(dbId, maxCount);
        List<GenericServerEvent> genericServerEvents = new ArrayList<>();
        events.forEach(e ->
                genericServerEvents.add(new GenericServerEvent(e.getId(), e.getCreateTimestampUtc(), e.getEventType(), e.getPayload())));
        return genericServerEvents;
    }
}
