package cloud.eventpump.examples.TransactionalClientServer.Server.eventPump;


import cloud.eventpump.server.EventAdapter;

class GenericServerEventAdapter implements EventAdapter<GenericServerEvent> {
    @Override
    public long getEventId(GenericServerEvent epEvent) {
        return epEvent.getEventId();
    }

    @Override
    public long getTimestampUTC(GenericServerEvent epEvent) {
        return epEvent.getCreateUtcTs();
    }
}
