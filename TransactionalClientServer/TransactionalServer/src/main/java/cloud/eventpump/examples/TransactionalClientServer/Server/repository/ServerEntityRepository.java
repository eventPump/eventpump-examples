package cloud.eventpump.examples.TransactionalClientServer.Server.repository;

import org.springframework.data.repository.CrudRepository;

public interface ServerEntityRepository extends CrudRepository<ServerEntity, String> {

}
