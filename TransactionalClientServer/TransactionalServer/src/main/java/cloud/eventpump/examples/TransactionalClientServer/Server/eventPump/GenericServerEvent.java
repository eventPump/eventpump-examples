package cloud.eventpump.examples.TransactionalClientServer.Server.eventPump;

public class GenericServerEvent {

    private long eventId;
    private long createUtcTs;
    private String serverEventType;
    private String payload;

    public GenericServerEvent(long eventId, long createUtcTs, String serverEventType, String payload) {
        this.eventId = eventId;
        this.createUtcTs = createUtcTs;
        this.serverEventType = serverEventType;
        this.payload = payload;
    }

    public long getEventId() {
        return eventId;
    }

    public long getCreateUtcTs() {
        return createUtcTs;
    }

    public String getServerEventType() {
        return serverEventType;
    }

    public String getPayload() {
        return payload;
    }
}
