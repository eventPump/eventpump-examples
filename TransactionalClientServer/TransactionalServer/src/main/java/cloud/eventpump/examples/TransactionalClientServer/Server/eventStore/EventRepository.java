package cloud.eventpump.examples.TransactionalClientServer.Server.eventStore;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EventRepository extends CrudRepository<Event, Long> {

    @Query(value = "SELECT * FROM Event event WHERE event.id > :dbId ORDER BY event.id LIMIT :maxCount", nativeQuery = true)
    List<Event> findEvents(@Param("dbId") long dbId, @Param("maxCount") long maxCount);

}
