package cloud.eventpump.examples.TransactionalClientServer.Server.domainEvents;

import lombok.Getter;

@Getter
public class ServerEntityCreated extends ServerEvent {

    private final String unchangeableField;
    private final int field1;
    private final String field2;

    public ServerEntityCreated(String id, String unchangeableField, int field1, String field2) {
        super(id, "EntityCreated");
        this.unchangeableField = unchangeableField;
        this.field1 = field1;
        this.field2 = field2;
    }

}
