package cloud.eventpump.examples.TransactionalClientServer.Server.repository;

import cloud.eventpump.examples.TransactionalClientServer.Server.businessLogic.ServerAggregate;
import cloud.eventpump.examples.TransactionalClientServer.Server.eventStore.EventStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
public class ServerRepository {

    @Autowired
    private ServerEntityRepository serverEntityRepository;

    @Autowired
    private EventStore eventStore;

    public void storeCreated(ServerAggregate aggregate) {
        ServerEntity newServerEntity = new ServerEntity(
                aggregate.getId(),
                aggregate.getUnchangeableField(),
                aggregate.getField1(),
                aggregate.getField2());
        serverEntityRepository.save(newServerEntity);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void store(ServerAggregate aggregate) {
        Optional<ServerEntity> serverEntity = serverEntityRepository.findById(aggregate.getId());
        if (serverEntity.isPresent()) {
            ServerEntity storedServerEntity = serverEntity.get();
            storedServerEntity.setUnchangeableField(aggregate.getUnchangeableField());
            storedServerEntity.setField1(aggregate.getField1());
            storedServerEntity.setField2(aggregate.getField2());
        } else {
            throw new RuntimeException("ServerEntity not found id:" + aggregate.getId());
        }
    }

    public ServerAggregate read(String id) {
        Optional<ServerEntity> serverEntity = serverEntityRepository.findById(id);
        if (serverEntity.isPresent()) {
            ServerEntity storedServerEntity = serverEntity.get();
            return new ServerAggregate(eventStore,
                    storedServerEntity.getId(),
                    storedServerEntity.getUnchangeableField(),
                    storedServerEntity.getField1(),
                    storedServerEntity.getField2(),
                    false);
        } else {
            throw new RuntimeException("ServerEntity not found id:" + id);
        }
    }


}
