package cloud.eventpump.examples.TransactionalClientServer.Server;

import cloud.eventpump.examples.TransactionalClientServer.Server.businessLogic.ServerAggregate;
import cloud.eventpump.examples.TransactionalClientServer.Server.eventStore.EventStore;
import cloud.eventpump.examples.TransactionalClientServer.Server.repository.ServerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ServerApplicationService {

    @Autowired
    private ServerRepository serverRepository;

    @Autowired
    private EventStore eventStore;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String create(String unchangeableField) {
        ServerAggregate serverAggregate = new ServerAggregate(eventStore);
        serverAggregate.create(unchangeableField);
        serverRepository.storeCreated(serverAggregate);
        return serverAggregate.getId() ;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void businessLogic1(String id, int value) {
        ServerAggregate serverAggregate = serverRepository.read(id);
        serverAggregate.businessLogic1(value);
        serverRepository.store(serverAggregate);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void businessLogic2(String id, String value) {
        ServerAggregate serverAggregate = serverRepository.read(id);
        serverAggregate.businessLogic2(value);
        serverRepository.store(serverAggregate);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void delete(String id) {
        ServerAggregate serverAggregate = serverRepository.read(id);
        serverAggregate.delete();
        serverRepository.store(serverAggregate);
    }

}
