package cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers;

import cloud.eventpump.examples.TransactionalClientServer.Client.ClientApplicationService;
import cloud.eventpump.examples.TransactionalClientServer.Client.DBDisplayService;
import cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.eventHandlerHost.ClientEvent;
import cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.eventHandlerHost.EventHandlerHost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class BusinessLogic1Handler extends ClientEvent {

    @Autowired
    private EventHandlerHost eventHandlerHost;

    @Autowired
    private ClientApplicationService clientApplicationService;

    @Autowired
    private DBDisplayService dbDisplayService;

    private boolean throwException = false;

    public void setThrowException(boolean throwException) {
        this.throwException = throwException;
    }

    @PostConstruct
    public void register() {
        eventHandlerHost.register("BusinessLogic1Applied", ClientEventBusinessLogic1Applied.class, this::processEvent);
    }

    public void processEvent(ClientEventBusinessLogic1Applied clientEventBusinessLogic1Applied) {
        if (throwException) {
            throw new RuntimeException("Exception intentionally thrown during event handling");
        }
        clientApplicationService.externalbusinessLogic1(
                clientEventBusinessLogic1Applied.getId(),
                clientEventBusinessLogic1Applied.field1);
        dbDisplayService.showDataBase("ClientEventBusinessLogic1Applied");
    }

    private static class ClientEventBusinessLogic1Applied extends ClientEvent {
        private int field1;
    }

}
