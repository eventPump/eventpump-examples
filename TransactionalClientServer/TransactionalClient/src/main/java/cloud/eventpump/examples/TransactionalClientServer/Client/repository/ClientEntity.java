package cloud.eventpump.examples.TransactionalClientServer.Client.repository;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class ClientEntity {

    @Id
    private String id;

    private int serverField1;
    private String serverExternalInfo;
    private String additionalClientField;
}
