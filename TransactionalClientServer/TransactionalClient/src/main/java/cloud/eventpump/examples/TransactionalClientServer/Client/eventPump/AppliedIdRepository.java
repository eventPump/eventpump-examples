package cloud.eventpump.examples.TransactionalClientServer.Client.eventPump;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppliedIdRepository extends CrudRepository<AppliedId, Long> {
}
