package cloud.eventpump.examples.TransactionalClientServer.Client.eventPump;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@ToString
public class AppliedId {

    @Id
    private Long id;
    private long appliedId;

    public AppliedId(Long id) {
        this.id = id;
    }
}
