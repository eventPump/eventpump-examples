package cloud.eventpump.examples.TransactionalClientServer.Client;

import cloud.eventpump.examples.TransactionalClientServer.Client.businessLogic.ClientAggregate;
import cloud.eventpump.examples.TransactionalClientServer.Client.repository.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Component
public class ClientApplicationService {

    @Autowired
    private Repository repository;

    // local BusinessLogic

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void clientbusinessLogic(String id, String value) {
        ClientAggregate clientAggregate = repository.read(id);
        clientAggregate.clientbusinessLogic(value);
        repository.store(clientAggregate);
    }


    // remote events BusinessLogic

    @Transactional(propagation = Propagation.REQUIRED)
    public void create(String id, int serverField1, String serverExternalInfo) {
        ClientAggregate clientAggregate = new ClientAggregate();
        clientAggregate.create(id, serverField1, serverExternalInfo);
        repository.storeCreated(clientAggregate);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void externalbusinessLogic1(String id, int serverField1) {
        ClientAggregate clientAggregate = repository.read(id);
        clientAggregate.externalbusinessLogic1(serverField1);
        repository.store(clientAggregate);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(String id) {
        ClientAggregate clientAggregate = repository.read(id);
        clientAggregate.delete();
        repository.store(clientAggregate);
    }

}
