package cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers;


import cloud.eventpump.examples.TransactionalClientServer.Client.ClientApplicationService;
import cloud.eventpump.examples.TransactionalClientServer.Client.DBDisplayService;
import cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.eventHandlerHost.ClientEvent;
import cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.eventHandlerHost.EventHandlerHost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


@Component
public class EntityCreatedHandler {

    @Autowired
    private EventHandlerHost eventHandlerHost;

    @Autowired
    private ClientApplicationService clientApplicationService;

    @Autowired
    private DBDisplayService dbDisplayService ;

    @PostConstruct
    public void register() {
        eventHandlerHost.register("EntityCreated", ClientEventEntityCreated.class, this::processEvent);
    }

    public void processEvent(ClientEventEntityCreated clientEventEntityCreated) {
        clientApplicationService.create(
                clientEventEntityCreated.getId(),
                clientEventEntityCreated.field1,
                clientEventEntityCreated.unchangeableField);
        dbDisplayService.showDataBase("ClientEventEntityCreated");
    }

    private static class ClientEventEntityCreated extends ClientEvent {
        // public because of jackson
        public String unchangeableField;
        public int field1;
        // there is no field2
    }

}
