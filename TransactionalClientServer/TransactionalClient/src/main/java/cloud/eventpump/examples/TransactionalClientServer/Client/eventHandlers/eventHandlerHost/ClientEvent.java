package cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.eventHandlerHost;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class ClientEvent {

    @JsonIgnore
    private long eventId = -1;

    @JsonIgnore
    private long createUtcTs = -1;

    private String id;

    public void setHeaderValues(long eventId, long createUtcTs) {
        this.eventId = eventId;
        this.createUtcTs = createUtcTs;
    }

    public long getEventId() {
        return eventId;
    }

    public long getCreateUtcTs() {
        return createUtcTs;
    }

    public String getId() {
        return id;
    }
}
