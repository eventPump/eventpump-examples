package cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers;

import cloud.eventpump.examples.TransactionalClientServer.Client.ClientApplicationService;
import cloud.eventpump.examples.TransactionalClientServer.Client.DBDisplayService;
import cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.eventHandlerHost.ClientEvent;
import cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.eventHandlerHost.EventHandlerHost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class BusinessLogic2Handler {

    @Autowired
    private EventHandlerHost eventHandlerHost;

    @Autowired
    private ClientApplicationService clientApplicationService;

    @Autowired
    private DBDisplayService dbDisplayService ;

    @PostConstruct
    public void register() {
        eventHandlerHost.register("BusinessLogic2Applied", ClientEventBusinessLogic2Applied.class, this::processEvent);
    }

    public void processEvent(ClientEventBusinessLogic2Applied clientEventBusinessLogic2Applied) {
        // no businessLogic action but appliedId have been updated
        dbDisplayService.showDataBase("ClientEventBusinessLogic2Applied" ) ;
    }

    private static class ClientEventBusinessLogic2Applied extends ClientEvent {
        private String field2;
    }


}
