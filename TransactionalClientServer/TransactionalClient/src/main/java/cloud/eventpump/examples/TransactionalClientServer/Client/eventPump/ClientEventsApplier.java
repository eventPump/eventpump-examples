package cloud.eventpump.examples.TransactionalClientServer.Client.eventPump;

import cloud.eventpump.common.EventsApplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClientEventsApplier implements EventsApplier<GenericClientEvent> {

    private static long APPLIED_ID_ID = 1;

    @Autowired
    private AppliedIdRepository appliedIdRepository;

    @Autowired
    private SingleEventApplier singleEventApplier;

    @Override
    public long findLastAppliedDBId() {
        return appliedIdRepository.findById(APPLIED_ID_ID)
                .map(AppliedId::getAppliedId)
                .orElse(0L);
    }

    @Override
    public long applyEvents(List<GenericClientEvent> list) {
        // Only to satisfy Spring transaction handling
        list.forEach(singleEventApplier::applyEvent);
        return list.get(list.size() - 1).getId();
    }

}
