package cloud.eventpump.examples.TransactionalClientServer.Client.eventPump;

import cloud.eventpump.tcpClient.TcpClientConfig;
import cloud.eventpump.tcpClient.TcpEventsPumpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.InetSocketAddress;

@Component
public class EventPumpClient {


//    public <Event extends ClientEvent> void register(String eventType, Class<Event> eventClass, EventHandler<Event> eventEventHandler) {
//        eventHandlerHost.register(eventType, eventClass, eventEventHandler);
//    }

    @Value("${ep.server.address}")
    private String epServerAddress;
    @Value("${ep.server.port}")
    private int epServerPort;
    @Value("${ep.client.name}")
    private String epClientName;

    @Autowired
    private ClientEventsApplier clientEventsApplier;

    @PostConstruct
    public void start() {
        TcpClientConfig<GenericClientEvent> clientConfig = TcpClientConfig.create(new GenericClientEventUnmarshaller());
        clientConfig.setEventUnmarshaller(new GenericClientEventUnmarshaller());
        clientConfig.setServer(new InetSocketAddress(epServerAddress, epServerPort));
        clientConfig.setClientName(epClientName);
        clientConfig.setRepeatConnectionBasicDelay(100);
        clientConfig.setRepeatConnectionMaxDelay(3000);

        TcpEventsPumpClient<GenericClientEvent> epEventTcpEventsPumpClient = new TcpEventsPumpClient<>(clientEventsApplier, clientConfig);
        epEventTcpEventsPumpClient.startContinuous();
    }

}
