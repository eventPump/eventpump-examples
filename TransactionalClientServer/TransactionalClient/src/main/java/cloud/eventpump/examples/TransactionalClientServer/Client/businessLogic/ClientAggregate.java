package cloud.eventpump.examples.TransactionalClientServer.Client.businessLogic;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ClientAggregate {

    private String id;
    private int serverField1;
    private String serverExternalInfo;
    private String additionalClientField;

    private boolean deleted;

    public void create(String id, int serverField1, String serverUnchangeableField) {
        this.id = id;
        this.serverField1 = serverField1;
        this.serverExternalInfo = serverUnchangeableField;
        this.additionalClientField = null;
    }

    public void clientbusinessLogic(String value) {
        additionalClientField = value;
    }

    public void externalbusinessLogic1(int serverField1) {
        this.serverField1 = serverField1;
    }

    public void delete() {
        if (deleted) {
            throw new RuntimeException("Entity is deleted id" + id);
        }
        deleted = true;
    }


}
