package cloud.eventpump.examples.TransactionalClientServer.Client;

import cloud.eventpump.examples.TransactionalClientServer.Client.eventPump.AppliedId;
import cloud.eventpump.examples.TransactionalClientServer.Client.eventPump.AppliedIdRepository;
import cloud.eventpump.examples.TransactionalClientServer.Client.repository.ClientEntity;
import cloud.eventpump.examples.TransactionalClientServer.Client.repository.ServerEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class DBDisplayService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ServerEntityRepository serverEntityRepository;

    @Autowired
    private AppliedIdRepository appliedIdRepository;


    public String findEntity(int serverField1) {
        List<ClientEntity> byServerField1 = serverEntityRepository.findByServerField1(serverField1);
        if (byServerField1.isEmpty()) {
            return null;
        } else {
            return byServerField1.get(0).getId();
        }
    }

    public void showDataBase(String title) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n" + title +"\n");
        sb.append("\tAppliedId:" + appliedIdRepository.findById(1L).map(AppliedId::getAppliedId).orElse(0L));
        serverEntityRepository.findAll().forEach(e -> sb.append("\n\t" + e));
        logger.info(sb.toString());
    }


}
