package cloud.eventpump.examples.TransactionalClientServer.Client.eventPump;

import cloud.eventpump.common.EventsApplier;
import cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.eventHandlerHost.EventHandlerHost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class SingleEventApplier{

    private static long APPLIED_ID_ID = 1;

    @Autowired
    private AppliedIdRepository appliedIdRepository;

    @Autowired
    private EventHandlerHost eventHandlerHost;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void applyEvent(GenericClientEvent e) {
        updateAppliedId(e);
        handleEvent(e);
    }

    private void updateAppliedId(GenericClientEvent e) {
        AppliedId appliedId = appliedIdRepository.findById(APPLIED_ID_ID).orElse(new AppliedId(APPLIED_ID_ID));
        appliedId.setAppliedId(e.getId());
        appliedIdRepository.save(appliedId);
    }

    private void handleEvent(GenericClientEvent e) {
        eventHandlerHost.process(e);
    }

}
