package cloud.eventpump.examples.TransactionalClientServer.Client;

import cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.BusinessLogic1Handler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//java -jar target/TransactionalClient-0.0.1-SNAPSHOT.jar --throwExceptionDuringEventHandling
//java -Dspring.jpa.hibernate.ddl-auto=update -jar target/TransactionalClient-0.0.1-SNAPSHOT.jar

@SpringBootApplication
public class TransactionalClientApplication implements ApplicationRunner {

    @Autowired
    private ClientApplicationService clientApplicationService;

    @Autowired
    private BusinessLogic1Handler businessLogic1Handler;
    // Just to set that exception should be thrown

    @Autowired
    private DBDisplayService dbDisplayService ;

    public static void main(String[] args) {
        SpringApplication.run(TransactionalClientApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws InterruptedException {
        if (args.containsOption("throwExceptionDuringEventHandling")) {
            businessLogic1Handler.setThrowException(true);
        }

        String id;
        while ((id = dbDisplayService.findEntity(0)) == null) {
            Thread.sleep(100);
        }
        clientApplicationService.clientbusinessLogic(id, "***-changed-***");
        dbDisplayService.showDataBase("clientbusinessLogic");
    }

}
