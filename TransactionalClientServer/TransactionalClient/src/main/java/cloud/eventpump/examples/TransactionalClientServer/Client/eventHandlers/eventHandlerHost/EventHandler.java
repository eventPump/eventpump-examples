package cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.eventHandlerHost;


import cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.eventHandlerHost.ClientEvent;

public interface EventHandler<Event extends ClientEvent> {

    void processEvent(Event event);
}
