package cloud.eventpump.examples.TransactionalClientServer.Client.repository;

import cloud.eventpump.examples.TransactionalClientServer.Client.businessLogic.ClientAggregate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
public class Repository {

    @Autowired
    private ServerEntityRepository serverEntityRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    public void storeCreated(ClientAggregate aggregate) {
        ClientEntity newClientEntity = new ClientEntity(
                aggregate.getId(),
                aggregate.getServerField1(),
                aggregate.getServerExternalInfo(),
                aggregate.getAdditionalClientField());
        serverEntityRepository.save(newClientEntity);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void store(ClientAggregate aggregate) {
        Optional<ClientEntity> serverEntity = serverEntityRepository.findById(aggregate.getId());
        if (serverEntity.isPresent()) {
            ClientEntity storedServerEntity = serverEntity.get();
            if ( !aggregate.isDeleted() ) {
                storedServerEntity.setServerField1(aggregate.getServerField1());
                storedServerEntity.setServerExternalInfo(aggregate.getServerExternalInfo());
                storedServerEntity.setAdditionalClientField(aggregate.getAdditionalClientField());
            } else {
                serverEntityRepository.delete(storedServerEntity);
            }
        } else {
            throw new RuntimeException("ClientEntity not found id:" + aggregate.getId());
        }
    }

    public ClientAggregate read(String id) {
        Optional<ClientEntity> clientEntity = serverEntityRepository.findById(id);
        if (clientEntity.isPresent()) {
            ClientEntity ce = clientEntity.get();
            return new ClientAggregate(
                    ce.getId(),
                    ce.getServerField1(),
                    ce.getServerExternalInfo(),
                    ce.getAdditionalClientField(),
                    false);
        } else {
            throw new RuntimeException("ClientEntity not found id:" + id);
        }
    }

}
