package cloud.eventpump.examples.TransactionalClientServer.Client.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ServerEntityRepository extends CrudRepository<ClientEntity, String> {

    List<ClientEntity> findByServerField1(int serverField1);

}
