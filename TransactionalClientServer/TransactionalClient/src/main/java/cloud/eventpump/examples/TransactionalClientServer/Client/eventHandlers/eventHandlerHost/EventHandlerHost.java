package cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.eventHandlerHost;

import cloud.eventpump.examples.TransactionalClientServer.Client.eventPump.GenericClientEvent;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class EventHandlerHost {

    private final Map<String, EventHandlerWithClass<? extends ClientEvent>> eventTypeToEventHandler = new HashMap<>();
    private final ObjectMapper mapper;

    public EventHandlerHost() {
        this.mapper = new ObjectMapper();
        this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public <Event extends ClientEvent> void register(String eventType, Class<Event> eventClass, EventHandler<Event> eventEventHandler) {
        eventTypeToEventHandler.put(eventType, new EventHandlerWithClass<>(eventEventHandler, eventClass));
    }

    public void process(GenericClientEvent genericClientEvent) {
        EventHandlerWithClass<? extends ClientEvent> eventHandlerWithClass = eventTypeToEventHandler.get(genericClientEvent.getEventType());
        if (eventHandlerWithClass != null) {
            processHandled(genericClientEvent, eventHandlerWithClass);
        } else {
            throw new RuntimeException("Unhandled event type:" + genericClientEvent.getEventType());
        }
    }

    private void processHandled(GenericClientEvent genericClientEvent, EventHandlerWithClass<? extends ClientEvent> eventHandlerWithClass) {
        ClientEvent clientEvent;
        try {
            clientEvent = mapper.readValue(genericClientEvent.getPayload(), eventHandlerWithClass.eventClass);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        clientEvent.setHeaderValues(genericClientEvent.getId(), genericClientEvent.getCreateUtcTs());
        eventHandlerWithClass.eventEventHandler.processEvent(clientEvent);
    }


    private static class EventHandlerWithClass<Event extends ClientEvent> {
        EventHandler eventEventHandler;
        Class<Event> eventClass;

        public EventHandlerWithClass(EventHandler<Event> eventEventHandler, Class<Event> eventClass) {
            this.eventEventHandler = eventEventHandler;
            this.eventClass = eventClass;
        }
    }
}
