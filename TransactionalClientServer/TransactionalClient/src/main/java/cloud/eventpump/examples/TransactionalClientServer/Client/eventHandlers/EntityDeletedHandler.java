package cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers;

import cloud.eventpump.examples.TransactionalClientServer.Client.ClientApplicationService;
import cloud.eventpump.examples.TransactionalClientServer.Client.DBDisplayService;
import cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.eventHandlerHost.ClientEvent;
import cloud.eventpump.examples.TransactionalClientServer.Client.eventHandlers.eventHandlerHost.EventHandlerHost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class EntityDeletedHandler extends ClientEvent {

    @Autowired
    private EventHandlerHost eventHandlerHost;

    @Autowired
    private ClientApplicationService clientApplicationService;

    @Autowired
    private DBDisplayService dbDisplayService;

    @PostConstruct
    public void register() {
        eventHandlerHost.register("EntityDeleted", ClientEventEntityDeleted.class, this::processEvent);
    }

    public void processEvent(ClientEventEntityDeleted clientEventEntityDeleted) {
        clientApplicationService.delete(clientEventEntityDeleted.getId());
        dbDisplayService.showDataBase("ClientEventEntityDeleted");
    }

    private static class ClientEventEntityDeleted extends ClientEvent {
    }

}
